```flow
st=>start: Imputation-based meta-analysis workflow
for GWAS-GEA analyses using SNPs
e=>end: End
opl=>operation: Selection of panels
io1=>inputoutput: Input data: SNPs
subl=>subroutine: Reference panel
subl2=>subroutine: Recombination map
subl3=>subroutine: Quality filtering
cond=>condition: Whole genome   
sequencing
technology?
opl2=>operation: Genotype imputation
in IMPUTEv2
io=>operation: Data Filtering:
Missing rate <10%
MAF>0.05
opl3=>operation: GWAS in EMMAX/PyLMM
GEA in Sambada/LFMMv2/PyLMM
opl4=>operation: Meta-analysis (based on
  GWAS and GEA statistics, separately)
cond2=>condition: Heterogeneity
I2>0.25   
io2=>inputoutput: Input data:
Phenotype/environment data
opl5=>operation: meta
opl6=>operation: metasoft


st->opl->io1->cond    
cond(yes)->io->opl3->io2->opl4->cond2->e
cond(no)->opl2->subl(down)->subl2->subl3
cond2(yes)->opl6
cond2(no)->opl5(right)

```
