# Get the arguments as a character vector.
myargs = commandArgs(trailingOnly=TRUE)
myargs



# The first argument is the text for input.
flush = myargs[1]
set = myargs[2]
height = myargs[3]


#Read the EMMAX results
Panel1Budflush<-read.table(flush)
Panel1Budset<-read.table(set)
Panel1Height<-read.table(height)

#Calculate the q-values
library(devtools)
library(qvalue)

Panel1Budflushpvalue<-Panel1Budflush$V4
Panel1Budsetpvalue<-Panel1Budset$V4
Panel1Heightpvalue<-Panel1Height$V4

qvaluePanel1Budflush <- qvalue(p = Panel1Budflushpvalue)
qvaluePanel1Budset <- qvalue(p = Panel1Budsetpvalue)
qvaluePanel1Height <- qvalue(p = Panel1Heightpvalue)

#See the summary of the results
summary(qvaluePanel1Budflush)
summary(qvaluePanel1Budset)
summary(qvaluePanel1Height)

#Plot them in a histogram
hist(qvaluePanel1Budflush)
hist(qvaluePanel1Budset)
hist(qvaluePanel1Height)

#Plot them in a plot
plot(qvaluePanel1Budflush)
plot(qvaluePanel1Budset)
plot(qvaluePanel1Height)

#Merge the adjusted p-values in the original tables
Panel1BudflushLFDR<-cbind(Panel1Budflush, qvaluePanel1Budflush$lfdr)
Panel1BudsetLFDR<-cbind(Panel1Budset, qvaluePanel1Budset$lfdr)
Panel1HeightLFDR<-cbind(Panel1Height, qvaluePanel1Height$lfdr)

#Filter the significant SNPs (P<0.05)
Panel1BudflushLFDR0.05<-Panel1BudflushLFDR[Panel1BudflushLFDR$`qvaluePanel1Budflush$lfdr`<0.05,]
Panel1BudsetLFDR0.05<-Panel1BudsetLFDR[Panel1BudsetLFDR$`qvaluePanel1Budset$lfdr`<0.05,]
Panel1HeightLFDR0.05<-Panel1HeightLFDR[Panel1HeightLFDR$`qvaluePanel1Height$lfdr`<0.05,]

# save the significant SNPs in a text file
write.table(Panel1BudflushLFDR0.05,"Panel1BudflushLFDR0.05.txt", col.names=F,quote=F,sep=" ")

write.table(Panel1BudsetLFDR0.05,"Panel1BudsetLFDR0.05.txt", col.names=F,quote=F,sep=" ")

write.table(Panel1HeightLFDR0.05,"Panel1HeightLFDR0.05.txt", col.names=F,quote=F,sep=" ")
