A working galaxy wrapper for LinkImputeR (http://www.cultivatingdiversity.org/software.html)

The .ini file used with this wrapped script needs to include the lines:
[Input]
filename = workflow.vcf
[Output]
control = impute.xml

An explanation of the above annoyance:
LinkImputeR has the names of input and output files, as well as filtering information, defined inside a .ini file.

Let's look at an example.
A download of LinkImputeR includes the following test .ini file:

--- accuracy.ini ---
[Input]
filename = apple.vcf

[InputFilters]
maf=0.05
positionmissing = 0.7
exacthw=0.01

[Global]
depth = 2,3,4

[CaseFilters]
missing = 0.2,0.4,0.6

[Stats]
root = ./
level = table
eachmasked = yes

[Output]
control = impute.xml

[Log]
file = log.txt
level = debug

[Accuracy]
numbermasked = 1000
--- EOF accuracy.ini ---

LinkImputeR must be run in two stages. First it is run in accuracy mode. Running in accuracy mode looks like this:

java -jar LinkImputeR.jar -s INIFILE

Where INIFILE is the name of the ini config file.
Running LinkImputeR in accuracy mode outputs two files. A .dat file containing statistics on all possible imputation cases for you to pick from. Also a .xml 'control' file. 

The name of this .xml control file is defined inside the .ini file. Looking at the above pasted example of accuracy.ini we see the [Output] header and the line: control = impute.xml.

There are two issues specific to galaxy here. 
Take a look at some of the wrapper which deals with this xml control file output.

    <data label="LinkImputeR_Step1: XML control file"
	  name="impute.xml"
	  from_work_dir="impute.xml" />

Here we must tell galaxy where this output file is coming from. We use the from_work_dir="" line to tell galaxy that the XML control file will be found in the working directory with a specific name: impute.xml.
If a .ini configuration file has been passed to this wrapped script which does not contain this line, galaxy will be unable to find the correct file, and we will be unable to impute. 

Additionally, the .ini config file must include a filename for the vcf file to be used. LinkImputeR expects this file to be in the working directory, even though it does not take the file as a command line argument. In order to make sure that our wrapped LinkImputeR is able to open this file, we create a symlink from the input file and give it a name: workflow.vcf. 

Fixes?
* parse the .ini file and pull out names of each file. This would require the addition of a little python script which could read the .ini file. I dislike the idea of adding complexity to a wrapped tool like this.
* prompt the user to enter the filename and/or output name. This is bad user experience. The user (who likely has no idea the annoyances under which we are bound here) will not want to add a file and then be prompted to enter the name of the same file.
