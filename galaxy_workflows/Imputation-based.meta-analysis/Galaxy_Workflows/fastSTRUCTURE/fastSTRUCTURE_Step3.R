# Get the arguments as a character vector.
myargs = commandArgs(trailingOnly=TRUE)
myargs



# The first argument is the text for input.
IDfam = myargs[1]
numIndividuals = myargs[2]
k = myargs[3]
k
 fs <-read.table(IDfam, header=F)
        fs$assignedPop <- apply(fs, 1, which.max)
        intercept<-rep(1,numIndividuals)
        fsintercept<-cbind(intercept,fs)
        ID<-read.table(IDfam)
        fsID<-cbind(ID,fsintercept)
        fsPop<-fsID[c('V1', 'V2', 'intercept', 'assignedPop')]
        write.table(fsPop,"assignedPop.txt", col.names=F,quote=F,sep=" ")
