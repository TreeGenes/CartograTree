# Get the arguments as a character vector.
myargs = commandArgs(trailingOnly=TRUE)
myargs



# The first argument is the text for input.
meanQ = myargs[1]
V = myargs[2]
color = myargs[3]
plot = myargs[4]

K <-read.table(meanQ)
        Ksorted<-K[order(K[[V]]),]
        Ksortedmatrix<-as.matrix(Ksorted)
        tKsortedmatrix<-t(Ksortedmatrix)
        pdf(file = "fastSTRUCTUREplot.pdf", wi = 30, he = 15)
        barplot(tKsortedmatrix, col = c(color))