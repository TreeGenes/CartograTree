<tool id="qcfiltering5" name="SNP quality filtering. Step 5: Missingness per marker" version="0.1.0">
    <requirements>
        <requirement type="package" version="0.1.16">vcftools</requirement>
    </requirements>
    <command detect_errors="exit_code"><![CDATA[
        vcftools --vcf '$vcf_input' --max-missing '$maxmissing' --recode --recode-INFO-all > out.recode.vcf
    ]]></command>
    <inputs>
        <param type="data" name="vcf_input" format="vcf" label= "Output of Step 4"/>
        <param name="maxmissing" type="float" value="0.9" label="Remove SNPs with more than a given value of frequency missing individuals per genotype (see explanation below for clarification)" help="Recommended value: 0.9" />
    </inputs>
    <outputs>
        <data label="QCfiltering_5: Missigness per marker" name="out.recode.vcf" from_work_dir="out.recode.vcf" format="vcf" />
    </outputs>
    <help><![CDATA[
       This Quality Control Filtering workflow performs the standard data quality filtering of SNPs required for genotype-phenotype/genotype-environment association analysis (aka GWAS and GEA, respectively) and population genomics.
       This is the fifth step of the workflow, which removes SNPs with more than a 10% of missing individuals per genotype (or set a minimum frequency of individuals per genotype at 0.9). This is the threshold typically used by default. However, this threshold can be modified regarding the specific needs of the dataset and the study. It is important to follow the order of the steps recommended by this workflow. Changing the order can lead to an unnecessary loss of data. Nevertheless, some of the steps can be skipped regarding the specific needs of the dataset and the study. 
       This tool requires a vcf file as input file, containing the SNPs

       *Vcftools* program is used to perform this quality filtering step.

       
    ]]></help>
    <citations>
        <citation type="bibtex">
@misc{githublink,
  author = {Danecek, Petr et al},
  year = {2011},
  title = {The Variant Call Format and VCFtools},
  publisher = {},
  journal = {Bioinformatics},
  url = {https://vcftools.github.io/license.html},
}</citation>
    </citations>
</tool>