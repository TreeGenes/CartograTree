<tool id="qcfiltering7" name="SNP quality filtering. Step 7: Hardy-Weinberg equilibrium" version="0.1.0">
    <requirements>
        <requirement type="package" version="1.90.beta.4.4">plink</requirement>
    </requirements>
    <command detect_errors="exit_code"><![CDATA[
        plink --vcf '$vcf_input' --double-id --allow-extra-chr --set-missing-var-ids @:# --hwe '$hwe' --recode vcf --out out.recode
    ]]></command>
    <inputs>
        <param type="data" name="vcf_input" format="vcf" label="Output of Step 6"/>
        <param name="hwe" type="float" value="0.000001" label="Exclude markers that failure the Hardy-Weinberg test at a specified significance threshold" help="Recommended value: 0.000001" />
    </inputs>
    <outputs>
        <data label="QCfiltering_7: Hardy-Weinberg equilibrium" name="out.recode.vcf" from_work_dir="out.recode.vcf" format="vcf" />
    </outputs>
    <help><![CDATA[
       This Quality Control Filtering workflow performs the standard data quality filtering of SNPs required for genotype-phenotype/genotype-environment association analysis (aka GWAS and GEA, respectively) and population genomics.
       This is the seventh step of the workflow, which excludes markers that failure the Hardy-Weinberg test at a specified significance threshold. The significance threshold typically used by default is 0.000001. However, this threshold can be modified regarding the specific needs of the dataset and the study. This step is important in Quality Control since markers deviating strongly from HWE should be filtered out as likely genotyping errors. It is important to follow the order of the steps recommended by this workflow. Changing the order can lead to an unnecessary loss of data. Nevertheless, some of the steps can be skipped regarding the specific needs of the dataset and the study. 
       This tool requires a vcf.gz or vcf file as input file, containing the SNPs

       *Plink* program is used to perform this quality filtering step.

       
    ]]></help>
    <citations>
        <citation type="bibtex">
@misc{githublink,
  author = {Purcel, Shaun et al},
  year = {2007},
  title = {PLINK: a toolset for whole-genome association and population-based linkage analysis},
  publisher = {},
  journal = {American Journal of Human Genetics, 81},
  url = {http://pngu.mgh.harvard.edu/purcell/plink/},
}</citation>
    </citations>
</tool>