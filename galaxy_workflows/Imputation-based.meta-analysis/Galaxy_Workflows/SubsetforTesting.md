#Code to subset a vcf file containing SNPs for testing. Obtained from this tutorial: https://speciationgenomics.github.io/filtering_vcfs/

```
module load vcflib/1.0.0-rc1
module load bcftools/1.9
bcftools view SNP.vcf.gz | vcfrandomsample -r 0.012 > SNP_subset.vcf
```

#Code to subset a chromosome (or two or three): https://www.biostars.org/p/201603/ 
```
vcftools --gzvcf SNP2.vcf.gz --chr 1 --chr2 --recode --out SNP2_chr1_chr2
```
