<tool id="pca2" name="Principal Component Analysis. Step 2: Plotting" version="0.1.0">
    <requirements>
        <requirement type="set_environment">R_SCRIPT_PATH</requirement>
        <requirement type="package" version="3.5.1">R</requirement>
    </requirements>
    <command detect_errors="exit_code"><![CDATA[
        install.packages("tidyverse")
        # load tidyverse package
        library(tidyverse)

        # read in data
        pca <- read_table2('$PCAouteigenvec', col_names = FALSE)
        eigenval <- scan('$PCAouteigenval')

        # sort out the pca data
        # remove nuisance column
        pca <- pca[,-1]
        # set names
        names(pca)[1] <- "ind"
        names(pca)[2:ncol(pca)] <- paste0("PC", 1:(ncol(pca)-1))

        # first convert to percentage variance explained
        pve <- data.frame(PC = 1:20, pve = eigenval/sum(eigenval)*100)

        # make plot 
        a <- ggplot(pve, aes(PC, pve)) + geom_bar(stat = "identity")
        a + ylab("Percentage variance explained") + theme_light()

        # calculate the cumulative sum of the percentage variance explained
        cumsum(pve$pve)

        # plot pca
        pdf(file = PCA.pdf, wi = 9, he = 6)
        b <- ggplot(pca, aes(PC1, PC2)) + geom_point(size = 3)
        b <- b + coord_equal() + theme_light()
        b + xlab(paste0("PC1 (", signif(pve$pve[1], 3), "%)")) + ylab(paste0("PC2 (", signif(pve$pve[2], 3), "%)"))


    ]]></command>
    <inputs>
        <param type="data" name="PCAouteigenval" label= ".eigenval file (output of Step 1)" />
        <param type="data" name="PCAouteigenvec" label = ".eigenvec file (output of Step 1)" />
        <param type="data" name="IDpopulations" format="csv" label="CSV file" help="An extra .csv file must be provided. This .csv file must contain at least one column with the names of the populations to which each individual belongs. It may contain an optional extra column containing the individuals names. Population column has to be named as Population. The individual column can be named as desired. The individuals must be in the same order as the individuals in the .eigenvec file"/>
    </inputs>
    <outputs>
        <data label="PCA plot" name="PCA.pdf" from_work_dir="PCA.pdf" format="pdf" />
    </outputs>
    <help><![CDATA[
       This workflow performs a Principal Component Analysis (PCA) as a means of exploratory analysis of the population structure.
       This is the second step of the workflow, which plots the PCA in a PDF file
       This tool requires an .eigenval and an .eigenvec file as input files, which are part of the outputs from the Step 1 of this workflow. In addition, an extra "vcf" file has to be included as input, containing the population names to which each individual belongs. 

       The R package *tidyverse* is used to perform the plotting. 
    ]]></help>
    <citations>
        <citation type="bibtex">
 @misc{githublink,
  author = {Wickham, Hadley et al},
  year = {2019},
  title = {Welcome to the tidyverse},
  publisher = {},
  journal = {Journal of Open Source Software, 4(43), 1686},
  url = {https://doi.org/10.21105/joss.01686},
}</citation>
    </citations>
</tool>
