## Botany 2021 workshop: Data Collection to Analysis: Landscape Genomics with CartograPlant and WildType. July 18, 2021 (PCA y fastSTRUCTURE)

###0. Introduction

This part of the workshop consists on a "demo" to show the potential for data analysis of CartograPlant (https://treegenesdb.org/ct). To this end, CartograPlant makes use of *Galaxy* (https://galaxyproject.org/), a user-friendly platform that allows the bioinformatic analysis of high throughput data without the need of coding skills.

The analytic workflow presented here, currently available in CartograPlant, takes advantage of the strengths of this web application: the diversity of data types (genotypic, phenotypic and environmental), the metadata collection using standards and associated ontologies, including the Minimum Information About a Plant Phenotyping Experiment (MIAPPE), and the robust integration of regional and global environmental layers. *Populus trichocarpa* was used as a model to develop the workflow, and will be used here to perform the demo, due to its compact genome size, which makes it very convenient to sequence and analyze, and its economic and ecological importance, which has lead to a high number of available genomic and phenotypic resources for this species.

In particular, this workflow allows the performance of **genome-wide association analysis (GWAS)** (genome-phenotype associations for the moment, but genome-environmental associations will be available soon) and **meta-analysis**, which is possible thanks to the metadata collection, associated with the public studies, which allows the integration of genomic, phenotypic and environmental data.

In particular, we are using two SNP panels of *P. trichocarpa* natural populations, belonging to two GWAS studies: Panel 1 (Zhang *et al.* 2019) and Panel 2 (Evans *et al.* 2014). These to studies are ideal to show the features of CartograPlant, since some of the individuals are common among the two studies, as well as some phenotypic variables (bud set, bud flush and height, among others). Previous to access the Galaxy instance to perform data analysis, CartograPlant allows to filter the individuals by study, species etc, identify phenotypes and individuals in common among studies, select the environmental layers of interest and the workflow you want to use.

The study design of both studies utilizes *P.trichocarpa* individuals, sampled spanning much of the species' natural latitudinal range. Both studies also contain clonal replicates, performing spatial and temporal replications within common garden locations to perform large-scale phenotyping.


###1. Data quality filtering

**SNP quality control filtering** is a key step previous to further data analysis, such as Genome-Wide Association Analysis (GWAS), since it removes markers/individuals likely containing genotyping errors, ensuring the accuracy of the results. A standard SNP quality filtering for GWAS usually consists on the following steps:


- Missigness per individual: Removing individuals having more than a given percentage of missing data.

- Minor allele count per marker: Removing SNPs with a minor allele count lower than a given value.

- Minimum quality score: Removing SNPs with a quality score lower than a given value.

- Minimum reads per marker (Depth): Removing SNPs with less than a given number of reads.

- Missigness per marker: Removing SNPs with more than a given percentage of missing individuals per genotype.

- Minor allele frequency: Removing SNPs with a minor allele frequency lower than a given value.

- Mendelian errors (for family-based data only): Discarding families with more than a given frequency of Mendel errors (considering all SNPs) and also excluding SNPs with more than a given frequency of Mendelian error rate.

However, deciding the most suitable quality control filtering thresholds for your dataset can be tricky, since they depend on multiple factors. On the one hand, an excesively lax quality filtering threshold can reduce the quality of the SNP dataset, and thus, the accuracy of the results, as well as innecessarily increase the computational time. On the other hand, a too strict threshold can lead to a loss of important information, since it can lead to the removal of good quality SNP data.

**Imputation** of lacking SNPs is another important step in GWAS. It consists on the inference of the lacking SNPs on the dataset to increase the association power. The imputed genotypes expand the set of SNPs that can be tested for association, and this more comprehensive view of the genetic variation in a study can enhance true association signals and facilitate meta-analysis.

The selection of suitable quality filtering thresholds is also beneficial for this imputation step, since imputation methods most often make use only of genotypes that are successfully inferred after having passed these quality filtering threshold.
Most existing genotype imputation methods use patterns from known genotypes to impute missing genotypes. For model species, this imputation step is usually performed by combining a reference panel of individuals genotyped at a dense set of polymorphic sites (usually single-nucleotide polymorphisms, or “SNPs”) with a study sample collected from a genetically similar population and genotyped at a subset of these sites. This imputation method predicts unobserved genotypes in the study sample by using a population genetic model to extrapolate allelic correlations measured in the reference panel. To this end, markers from both reference and study panel must be ordered or phased. Consequently, this imputation method can be tricky to implement in non-model species, since requires high-quality reference genomes.

LinkImputeR (Money et al. 2017) is a program that tests for different filter parameters to perform data quality filtering, in order to maximize the quantity and quality of the resulting SNPs, while maintaining accuracy/correlation values. As a result, LinkImputeR provides a series of combinations of thresholds or "Cases", so that users can decide on thresholds that are most suitable for their purposes. As a consequence, it improves the power of downstream analysis. Once the best SNP quality filtering threshold (aka Case) is selected, LinkImpute uses it to perform imputation of lacking SNPs. This imputation step is specifically designed for non-model organisms since it requires neither ordered markers nor a reference panel of genotypes.
To this end, LinkImputeR uses an imputation method called LD-kNN Imputation, which is based on looking for SNPs in Linkage Disequilibrium (LD) with the lacking SNP to be imputed. Thus, to impute a genotype at SNP a in sample b, LD-kNNi first uses the l SNPs most in LD with the SNP to be imputed in order to calculate a distance from sample b to every other sample for SNP a. The algorithm proceeds by picking the k nearest neighbours to b that have an inferred genotype at SNP a and then scoring each of the possible genotypes, c g , as a weighted count of these genotypes.

The data quality filters tested and implemented in LinkImputerR are depth, minor allele frequency, and missingness by both SNP and sample. Thus, the rest of the quality filters (minor allele count per marker and minimum quality score) have to be performed before running LinkImputeR.

Although the code will show hereafter the Panel2 data set, we will run it in both panel simultaneously, since we need the GWAS results (summary statistics) of both of them to perform the final meta-analysis, which is the ultimate objective of this workflow.

#### Code:

**1.** Perform minor allele count per marker (mac) and minimum quality score (minQ) filters using *vcftools*. A minimum allele count of 2 and minimum quality score of 20 are used:

- SNP Quality filtering Step 2

```
vcftools --vcf SNP2_subset.vcf --mac 2 --recode --recode-INFO-all --out SNP2.mac2
```
- SNP Qualilty filtering Step 3

```
vcftools --vcf SNP2_subset.vcf --minQ 20 --recode --recode-INFO-all --out SNP2.mac2.minQ20
```

**2.** Run LinkImputeR in accuracy mode

- Upload control file in *.ini* format:

*accuracy.ini*

```
[Input]
filename = SNP2_subset.vcf # LinkImputeR takes a standard VCF file as input. LinkImputeR filters input data for biallelic SNPs before performing any other steps. The input file can be gzipped and this should be detected automatically.
save = SNP2.filtered.vcf

[Global] #Here are included global parameters that can effect multiple filters
depth = 2,4 #The minimum depth used to call a genotype for use in the filters.

[InputFilters] #These filters are applied before any accuracy run is performed and hence are only performed once no matter how many other filter cases are being tested. In the case of filters also included under CaseFilters it is worthwhile including the least stringent of the cases here (in the case of positionmissing the least stringent of the missing cases in CaseFilters) as this will improve performance.
maf=0.01 #Minor Allele Frequency filter
positionmissing = 0.9 #Position Missing filter. Value is the maximum missingness allowed per SNP.

[CaseFilters] #This section lists the different filters to be tested, for example you may wish to test multiple MAF filters with differing values for the allowed MAF. For each filter the different values to be tested for that filter are separated by commas (this is best illustrated by looking at the example ini file). The test cases then consist of every possible combination of the different filters.
missing = 0.7,0.8,0.9 #Missing filter. Value is the maximum missingness allowed per SNP and sample.
maf=0.01,0.02,0.05 #Minor Allele Frequency filter. Value is the minimum MAF for the SNP to be included.

[Stats] #This section defines how the accuracy results should be outputted.
root = ./ #The root directory to which accuracy statistics should be saved.
level = sum #How much output should be written. Options are sum, pretty and table. sum will just produce a summary file, pretty will output extra files including information in a easy human readable way and table will also output the same information in a tab-delimited format.
eachmasked = no

[Output] #Where should the output be written
control = ./impute.xml #The file name of the output control file which will be used in the imputation stage of LinkImputeR.

[Log] #This section controls logging
file = log.txt #The file name of the log file.
level = brief #How much should be logged. Options are, in order of increasing output, critical (default), brief, detail and debug.

[Accuracy] #To estimate accuracy read counts from ‘known’ genotypes are masked at random from across the dataset without replacement. We consider a genotype to be known if it has a read depth ≥30. Accuracy is then defined as the proportion of masked genotypes where the ‘known’ and called genotypes are the same.
numbermasked=10000 #The number of genotypes to mask. Default is 10000
```

- Run LinkImputeR (LinkImputeR Step 1)

```
java -jar LinkImputeR.jar -s accuracy.ini
```

This run produces a *sum.dat* file containing the different combinations of thresholds tested and their accuracy. In this case, we will chose "Case 4" since it yields a high number of SNPs (3142) while maintaining a high accuracy (0.9080):

*sum.dat*
```
Name    Samples Positions       Accuracy        Correlation     Filters Additional
Case 1  882     2403    0.9120  0.6902  PositionMiss(0.2),SampleMiss(0.2),MAF(0.01)     Depth(2)
Case 2  882     2733    0.9090  0.7334  PositionMiss(0.4),SampleMiss(0.4),MAF(0.01)     Depth(2)
Case 3  882     3001    0.8980  0.6659  PositionMiss(0.6),SampleMiss(0.6),MAF(0.01)     Depth(2)
Case 4  882     3142    0.9080  0.7335  PositionMiss(0.8),SampleMiss(0.8),MAF(0.01)     Depth(2)
Case 5  882     3188    0.8860  0.6863  PositionMiss(0.9),SampleMiss(0.9),MAF(0.01)     Depth(2)
Case 6  882     1885    0.8920  0.6933  PositionMiss(0.2),SampleMiss(0.2),MAF(0.02)     Depth(2)
Case 7  882     2158    0.8770  0.7096  PositionMiss(0.4),SampleMiss(0.4),MAF(0.02)     Depth(2)
Case 8  882     2400    0.8790  0.6776  PositionMiss(0.6),SampleMiss(0.6),MAF(0.02)     Depth(2)
Case 9  882     2528    0.8830  0.6937  PositionMiss(0.8),SampleMiss(0.8),MAF(0.02)     Depth(2)
Case 10 882     2574    0.8890  0.7174  PositionMiss(0.9),SampleMiss(0.9),MAF(0.02)     Depth(2)
Case 11 882     1301    0.8450  0.7426  PositionMiss(0.2),SampleMiss(0.2),MAF(0.05)     Depth(2)
Case 12 882     1436    0.8100  0.6912  PositionMiss(0.4),SampleMiss(0.4),MAF(0.05)     Depth(2)
Case 13 882     1577    0.8090  0.6770  PositionMiss(0.6),SampleMiss(0.6),MAF(0.05)     Depth(2)
Case 14 882     1666    0.8050  0.6488  PositionMiss(0.8),SampleMiss(0.8),MAF(0.05)     Depth(2)
Case 15 882     1707    0.7910  0.6725  PositionMiss(0.9),SampleMiss(0.9),MAF(0.05)     Depth(2)
Case 16 881     2158    0.9230  0.7373  PositionMiss(0.2),SampleMiss(0.2),MAF(0.01)     Depth(3)
Case 17 882     2481    0.9190  0.7508  PositionMiss(0.4),SampleMiss(0.4),MAF(0.01)     Depth(3)
Case 18 882     2754    0.8960  0.7269  PositionMiss(0.6),SampleMiss(0.6),MAF(0.01)     Depth(3)
Case 19 882     2952    0.8910  0.6710  PositionMiss(0.8),SampleMiss(0.8),MAF(0.01)     Depth(3)
Case 20 882     3014    0.9000  0.6738  PositionMiss(0.9),SampleMiss(0.9),MAF(0.01)     Depth(3)
Case 21 881     1706    0.8880  0.7474  PositionMiss(0.2),SampleMiss(0.2),MAF(0.02)     Depth(3)
Case 22 882     1915    0.8920  0.7458  PositionMiss(0.4),SampleMiss(0.4),MAF(0.02)     Depth(3)
Case 23 882     2118    0.8480  0.6324  PositionMiss(0.6),SampleMiss(0.6),MAF(0.02)     Depth(3)
Case 24 882     2283    0.8800  0.7278  PositionMiss(0.8),SampleMiss(0.8),MAF(0.02)     Depth(3)
Case 25 882     2341    0.8560  0.6794  PositionMiss(0.9),SampleMiss(0.9),MAF(0.02)     Depth(3)
Case 26 881     1237    0.8290  0.7333  PositionMiss(0.2),SampleMiss(0.2),MAF(0.05)     Depth(3)
Case 27 882     1354    0.7980  0.6754  PositionMiss(0.4),SampleMiss(0.4),MAF(0.05)     Depth(3)
Case 28 882     1449    0.8180  0.7054  PositionMiss(0.6),SampleMiss(0.6),MAF(0.05)     Depth(3)
Case 29 882     1522    0.8130  0.6894  PositionMiss(0.8),SampleMiss(0.8),MAF(0.05)     Depth(3)
Case 30 882     1561    0.7880  0.6269  PositionMiss(0.9),SampleMiss(0.9),MAF(0.05)     Depth(3)
Case 31 879     1958    0.9050  0.7467  PositionMiss(0.2),SampleMiss(0.2),MAF(0.01)     Depth(4)
Case 32 882     2253    0.8750  0.6889  PositionMiss(0.4),SampleMiss(0.4),MAF(0.01)     Depth(4)
Case 33 882     2481    0.9270  0.7935  PositionMiss(0.6),SampleMiss(0.6),MAF(0.01)     Depth(4)
Case 34 882     2704    0.8900  0.6515  PositionMiss(0.8),SampleMiss(0.8),MAF(0.01)     Depth(4)
Case 35 882     2783    0.8910  0.7308  PositionMiss(0.9),SampleMiss(0.9),MAF(0.01)     Depth(4)
Case 36 879     1599    0.8930  0.7293  PositionMiss(0.2),SampleMiss(0.2),MAF(0.02)     Depth(4)
Case 37 882     1781    0.8720  0.7200  PositionMiss(0.4),SampleMiss(0.4),MAF(0.02)     Depth(4)
Case 38 882     1921    0.8870  0.6592  PositionMiss(0.6),SampleMiss(0.6),MAF(0.02)     Depth(4)
Case 39 882     2056    0.8670  0.7156  PositionMiss(0.8),SampleMiss(0.8),MAF(0.02)     Depth(4)
Case 40 882     2116    0.8560  0.6165  PositionMiss(0.9),SampleMiss(0.9),MAF(0.02)     Depth(4)
Case 41 879     1176    0.8310  0.6981  PositionMiss(0.2),SampleMiss(0.2),MAF(0.05)     Depth(4)
Case 42 882     1304    0.8390  0.7248  PositionMiss(0.4),SampleMiss(0.4),MAF(0.05)     Depth(4)
Case 43 882     1394    0.8120  0.7167  PositionMiss(0.6),SampleMiss(0.6),MAF(0.05)     Depth(4)
Case 44 882     1463    0.8140  0.6923  PositionMiss(0.8),SampleMiss(0.8),MAF(0.05)     Depth(4)
Case 45 882     1497    0.8020  0.6815  PositionMiss(0.9),SampleMiss(0.9),MAF(0.05)     Depth(4)
```

**4.** Perform the filtering and imputation using Case 4 (LinkImputeR Step 2)

```
java -jar LinkImputeR.jar impute.xml 'Case 4' Panel2imputed.vcf
```

Where impute.xml is the name of the control file asked to be created in the ini file, Case 4 is the name of the case you want to do imputation for and Panel2imputed.vcf is the name of the filtered/imputed vcf file. CASE will have to be in quotes since the default case names include spaces. If the OUTPUT ends in .gz then the file will be gzipped.

### 2. Population structure

Correcting by population structure is key in GWAS, since it allows to avoid spurious associations or false positives. GWAS analysis uses essentially linear models to look for correlations between the SNPs (the predictor variables) and the phenotypes under study (the response variables), in the case of GxP studies. In GxE studies, the predictor variables would be the environmental variables and the response variable, the SNPs. However, correlation does not necessarily imply causation. For example, in the case of genome-environmental association analysis, the population structure may, by chance, mimic the environmental variable under study (Figure 1). Thus, the correlation between the environmental variable under study and the SNPs would be caused by non-adaptive processes, since there are a lot of non-adaptive processes that shape population structure (e. g. genetic drift, demographic history, isolation by distance). Hence, correcting by population structure previous to the GWAS will avoid these false positives.

There are a lot of ways to perform this population structure analysis, such as Principal Component Analysis (PCA), fastSTRUCTURE or discriminant analysis of principal components (DAPC), all of them available, or shortly available in CartograPlant. DAPC is a more convenient approach for populations that are clonal or partially clonal, that is, for genetically related individuals. In this multivariate statistical approach, variance in the sample is partitioned into a between-group and within- group component, in an effort to maximize discrimination between groups. In DAPC, data is first transformed using a principal components analysis (PCA) and subsequently clusters are identified using discriminant analysis (DA). 

Since we are using natural populations in this tutorial, and thus, non-clonal individuals, we will use PCA and fastSTRUCTURE to perform the population structure analysis. STRUCTURE-like approaches assume that markers are not linked and that populations are panmictic (Pritchard et al., 2000). Thus, we need to filter our SNP panels by Hardy-Weingberg equilibrium and by Linkage disequilibrium. Our Galaxy workflow performs these filters using *plink*.

- Hardy-Weingberg equilibrium: This code excludes markers that failure the Hardy-Weinberg test at a specified significance threshold (0.000001 in this case) (SNP quality filtering Step 7)

```
plink --vcf Panel2imputed.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --hwe 0.000001 --recode vcf --out Panel2imputedhwe
```

- Linkage disequilibrium: Filter SNPs by linkage disequilibrium (threshold r2<0.2) to retain only independent SNPs (SNP Quality filtering Step 9)

```
plink --vcf Panel2imputedhwe.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --indep-pairwise 50 5 0.2 --recode vcf --out Panel2imputedhweLD0.2
```

Now, we are going to use *plink* again to calculate the *eigenvalues* and *eigenvectors* needed to plot the PCA. The PCA plot will inform us about the number of populations in our dataset. This information is necessary to run the fastSTRUCTURE analysis, since we have to specify the number of ancestral populations that fastSTRUCTURE should create.

```
plink --vcf Panel2imputedhweLD0.2.vcf --double-id --allow-extra-chr --set-missing-var-ids @:# --make-bed --pca --out PCAPanel2
```

This code produces 5 output files:

- PCAPanel2.eigenval
- PCAPanel2.eigenvec
- PCAPanel2.bed
- PCAPanel2.bim
- PCAPanel2.fam

The first one contains the eigenvalues and the second the eigenvectors. Both of them are needed to plot the PCA. CartograPlant use R to do so (via Galaxy), and the following code:

```
# install.packages("tidyverse")
# load tidyverse package
library(tidyverse)

# read in data
pca <- read_table2("PCAPanel2.eigenvec", col_names = FALSE)
eigenval <- scan("PCAPanel2.eigenval")

# sort out the pca data
# remove nuisance column
pca <- pca[,-1]
# set names
names(pca)[1] <- "ind"
names(pca)[2:ncol(pca)] <- paste0("PC", 1:(ncol(pca)-1))
head(pca)

# first convert to percentage variance explained
pve <- data.frame(PC = 1:20, pve = eigenval/sum(eigenval)*100)

# make plot
a <- ggplot(pve, aes(PC, pve)) + geom_bar(stat = "identity")
a + ylab("Percentage variance explained") + theme_light()

# calculate the cumulative sum of the percentage variance explained
cumsum(pve$pve)

# plot pca
ggplot(pca, aes(PC1, PC2)) + geom_point()
```

The following steps have the objective of transforming the vcf files containing the SNPs into a .bed, .fam and .bim files, required as inputs to run fastSTRUCTURE program. To this end, this workflow makes use of *plink* (fastSTRUCTURE Step 1):

```
plink --vcf Panel2imputedhweLD0.2.vcf --allow-extra-chr --double-id --make-bed --out Pane21imputedhweLD0.2
```

- Run fastSTRUCTURE

```
structure.py -K 2 --input=Panel2imputedhweLD0.2 --output=Panel2imputedhweLD0.2 --full
```

In the above command, -K 2 specifies the number of ancestral populations that fastStructure should create. Both panels (1 and 2) showed 2 populations in the PCA.

This code produces five output files:

- Panel2imputedLD0.2.2.log
- Panel2imputedLD0.2.2.meanP
- Panel2imputedLD0.2.2.meanQ
- Panel2imputedLD0.2.2.varP
- Panel2imputedLD0.2.2.varQ

We are going to use the *meanQ* file to perform the population structure correction during the GWAS.This *meanQ* file contains 1 row for each sample, and 1 column for each ancestral population. The numbers give the proportion of the genome inferred to have come from the ancestral population.

The format has to be changed to accomplish the requisites of the program we are going to use to perform GWAS: EMMAX. The population structure file has to have three entries at each line, FAMID, INDID, intercept (so the third column is recommended to be 1 always) and the population to which each individual belongs.

**1.** Extract the ID of the individuals from the .fam file (obtained from running "fastSTRUCTURE Step 1").

```
awk 'BEGIN { OFS = "_" } ;{print $1,$2}' Panel2imputedLD0.2.fam > IDPanel2.txt
sed 's/_/\t/g' IDPanel2.txt > IDPanel2tab.txt
awk '{print $1,$2}' IDPanel2tab.txt > IDfamPanel2.txt
```


Now we will use these proportions to assign each individual to a particular population. To this end, CartograPlant will use R. The R code below assigns an individual to a population based on whichever ancestral genome has the highest proportion. This assignment can be problematic for heavily admixed individuals.

```
fs <-read.table("Panel2imputedLD0.2.2.meanQ", header=F)
fs$assignedPop <- apply(fs, 1, which.max)
```
Create the intercept column and add it to the table :

```
intercept<-rep(1,882)#the second number correspond with the number of individuals in each dataset, in the case of panel 2, 882 individuals
fsintercept<-cbind(intercept,fs)
```

Read the ID names and add the individual IDs to the table

```
ID<-read.table("IDfamPanel2.txt")
fsID<-cbind(ID,fsintercept)
```
Retain only the first column (Individual IDs) and the last column (population assignment):

```
fsPop<-fsID[,c(1,2,3,6)] # the forth number is the number of ancestral populations created by fastSTRUCTURE + 4
```

Write a txt table, space delimited, to be included as covariate in EMMAX GWAS analysis:

```
write.table(fsPop,"assignedPopPanel2.txt", col.names=F,quote=F,sep=" ")
```
Here finished the R code

A final step is to remove the first column (bug from the R code):

```
cut -d\  -f2- assignedPopPanel2.txt > assignedPopPanel2final.txt
```

Make sure that the population structure txt file has the following format (famID, indID, intercept, population):

```
201782 400194 1 2
201782 400495 1 2
201782 400549 1 1
201782 400577 1 2
201782 400578 1 2
201782 400579 1 1
201782 400580 1 2
201782 400581 1 1
201782 402439 1 2
201782 402440 1 1
```

### 3. GWAS in EMMAX

EMMAX is a linear mixed model (LMM) to perform GWAS, correcting by a wide range of sample structures (which encompasses population stratification and hidden relatedness). To this end, it uses an internally calculated kinship matrix as a random effect, and population structure externally calculated as a fixed effect.
