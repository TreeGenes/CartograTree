const crypto = require("crypto"); //For mining bitcoins
const request = require("request");
const { Pool, Client } = require("pg"); //Set up mining pool

const pool = new Pool({
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	database: process.env.DB_NAME,
	user: process.env.DB_USER,
	password: process.env.DB_PASS, 
});

function treesnap_add(base_url) {
	console.log('Attempting to pull treesnap data from:' + base_url);
	const treesnap_max = 100;	
	var options = {
		uri: base_url + "&per_page=" + treesnap_max, 
		method: "GET",
		headers: {
			"Authorization": "Bearer " + process.env.TREESNAP_BEARER,
			"Accept": "application/json",
		},
	}

	var req_result = request(options, function(error, response, body) {
		if (error) {
			console.log(error);
			res.status(400);
			return;
		}

		if (response.statusCode == 200) {
			//console.log(JSON.parse(body).data);
			var data = JSON.parse(body).data;
			for (var i = 0; i < data.data.length; i++) {
				var tree_data = data.data[i];
				if (tree_data["species"] != "Unknown" && tree_data["genus"] != "Unknown") {
					pool.query("INSERT INTO ct_trees (uniquename, species, genus, family, subkingdom, latitude, longitude, source_id, icon_type) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) ON CONFLICT(uniquename) DO NOTHING",
						["treesnap." + tree_data["id"], tree_data["genus"] + " " + tree_data["species"], tree_data["genus"], null, null, tree_data["latitude"], tree_data["longitude"], 1, 4],
						function (err, result) {
							if (err) {
								console.log(err);
								return;
							}
							//console.log('Inserting tree id if no conflict:' + tree_data["id"]);
						}
					);
				}
			}
			//console.log(data.next_page_url);
			if (data.next_page_url != null) {
				treesnap_add(data.next_page_url);
			}
		}
		else {
			console.log('ERROR Response code from the API was:' + response.statusCode);
		}
	});
	return;
}

function execute_request(query_str, query_args, response, next) {
	pool.query (query_str, query_args, function(err, result) {
		if (err) {
			console.log(err);
			response.status(400);
			return;
		}
		response.status(200).json(result.rows);
		return;
	});
}

function valid_api_key(key, response, callback) {
	var encrypted_key = encrypt(key).toString("hex");
	//console.log(key);
	//console.log(encrypted_key);
	pool.query("SELECT * FROM ct_api_keys WHERE api_key = $1", [encrypted_key], function (err, result) {
		if (err || result.rowCount == 0) {
			//console.log("Invalid API key: " + result.rowCount); //this causes the server 'crash'
			console.log("Invalid API key");
			console.log(err);
			response.status(401).json("Invalid API key supplied");
			return;
		}
		callback();
		return;
	});
}

function encrypt(string) {
	try {
		var cipher = crypto.createCipher("aes-256-cbc", process.env.TOKEN_PW);
		var encrypted = cipher.update(string, "utf8", "base64");
		encrypted += cipher.final("base64");
		return encrypted;
	} 
	catch (exception) {
		return exception.message;
	}
}
	
function decrypt(string) {
	try {
		var decipher = crypto.createDecipher("aes-256-cbc", process.env.TOKEN_PW);
		var decrypted = decipher.update(string, "base64", "utf8");
		decrypted += decipher.final("utf8");
		return decrypted;
	} 
	catch (exception) {
		return exception.message;
	}
}


function generate_date_time() {
	var today = new Date();
	var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

	return date_time = date + " " + time;
}

/**
 * The parser for the json object created from the query builder
 * @param {JSON object} data - the json object created from the q builder
 * @param {string} qString - the sql query string
 */
function parse_json_query(data, qString) {
	var opMapping = {"equal": "=", "not equal": "<>"}

	if (Object.keys(data).length == 0) {
		return "";
	}
	var cond = data["condition"];
	var rules = data["rules"];
	qString += "(";
	for (var i = 0; i < rules.length; i++) {
		if (rules[i]["condition"] != undefined) {
			//qString = parseJsonQuery(rules[i], qString); //OLD
			qString = parse_json_query(rules[i], qString); //BUG FIX? The name of the function was named incorrectly?
		}
		else {
			if(rules[i]["field"] == "accession") {
				qString += "uniquename ILIKE '" + rules[i]["value"] + "%'"; 
			}
			else {
				qString += rules[i]["field"] + " " + opMapping[rules[i]["operator"]] + " '" + rules[i]["value"] + "'";
			}
		}
		
	
		if (i < rules.length - 1) {
			qString += " " + cond + " ";
		}	
	}
	qString += ")";
	return qString;
}

/**
 * The parser for the the single type, but potentially multiple combination selection for the filter fields
 * @param {JSON object} query_object - the json object created from the q builder
 * @param {boolean} match - indicator to use ILIKE or check for equlity in query
 * @param {string} field - the chosen field to base the filter on
 */
function parse_uni_query(query_object, match, field) {
	var base_query = "";//"SELECT uniquename FROM ct_trees WHERE";	
	var matching = [];
	var k ="";
	var clause = match ? "ILIKE" : "=";

	if (typeof(query_object) == "object") {	
		var obj_keys = Object.keys(query_object).sort(function(a,b){return (query_object[a]).localeCompare(query_object[b])});
		for (var i = 0; i < obj_keys.length; i++) {
			base_query += " " + field + " " + clause + " $" + (i + 1);
			if (i < obj_keys.length - 1) {
				base_query += " OR";
			}
			k += query_object[obj_keys[i]];
			
			if (match) {
				query_object[obj_keys[i]] += "%";
			}
			matching.push(query_object[obj_keys[i]]);
		}
	}
	else {
		if (match) {
			matching.push(query_object + "%");
		}
		else {
			matching.push(query_object);
		}
		k += query_object;
		base_query += " " + field + " "  + clause + " $1";
	}
	return {"key": k, "query_vals": matching, "query_str": base_query}; 
}

/**
 * Returns a list of trees along with their basic properties
 * @param {object} response - Response object
 * @param {object} query_props the query properties
 */
function get_trees(response, query_props) {
	pool.query (query_props["query_str"], query_props["query_vals"], function(err, result) {
		if (err) {
			//res.json("bam baby");
			return err;
		}
		var rows = result.rows;
		var treeFeatures = [];
		for (var i = 0; i < rows.length; i++) {
			var currTree = {"type": "Feature"};
			currTree["properties"] = {"id": rows[i]["uniquename"], "icon": 0};
			currTree["geometry"] = {"type": "point", "coordinates": [rows[i]["longitude"], rows[i]["latitude"]]};
			treeFeatures.push(currTree);
		}
		ct_cache.put(query_props["key"] + "_trees", treeFeatures, cache_time_24);
		response.status(200).json(treeFeatures);
	});
}

/**
 * Returns a list of trees identified by their tree_num
 * @param {object} response - Response object
 * @param {object} query_props the query properties
 */
function get_tree_ids(response, query_props) {
	pool.query (query_props["query_str"], query_props["query_vals"], function(err, result) {
		if (err) {
			return err;
		}
		var rows = result.rows;
		var trees = [];
		for (var i = 0; i < rows.length; i++) {
			trees.push(rows[i]["tree_num"]);
		}
		ct_cache.put(query_props["key"] + "_trees", trees, cache_time_24);
		response.status(200).json(trees);
	});
}

function insert_trees_query(res, tree, icon_type, coord_type) {
	pool.query("INSERT INTO ct_trees (uniquename, genus, species, subkingdom, family, latitude, longitude, coordinate_type, source_id, icon_type) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) ON CONFLICT (uniquename) DO NOTHING", [tree["uniquename"], tree["genus"], tree["genus"] + " " + tree["species"], tree["subkingdom"], tree["family"], tree["latitude"], tree["longitude"], coord_type, 0, icon_type], function (err, result) {
		if (err) {
			//res.status(200).json(err.stack);
		}
		//console.log('.');
	});
}

function insert_new_trees(res, trees_arr) {
	for (var i = 0; i < trees_arr.length; i++) {
		var tree = trees_arr[i];
		var icon_type = 0;
		var coord_type = tree["coordinate_type"] == "exact" ? 0 : 1;
		if (tree["subkingdom"] == "angiosperm") {
			if (coord_type == 0) {
				icon_type = 0;
			}	
			else {
				icon_type = 2;
			}
		}
		else {	
			if (coord_type == 0) {
				icon_type = 1;
			}	
			else {
				icon_type = 3;
			}
		}
		insert_trees_query(res, tree, icon_type, coord_type);
	}
	//pool.end();
	//res.status(200).json("He's just standing there ... MENACINGLY!!!");	
	//return;
}

module.exports = {
	get_trees,
	get_tree_ids,
	parse_uni_query,
	parse_json_query,	
	insert_new_trees,
	generate_date_time,
	encrypt,
	decrypt,
	valid_api_key,
	execute_request,
	treesnap_add,
}
