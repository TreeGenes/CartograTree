//nodejs libraries
const request = require("request");
const ct_cache = require("memory-cache");
var fs = require('fs');
//dependencies js
const config = require('./../config');
const utils = require("./../utils");
const query = require("./../query");
var pg_native_client = require('pg-native');
const cache_time_24 = 24*60*60*1000;

/**
 * Clears the cache and refreshes the materialized views to able to view all the newly added trees to cartogratree based on tpps submissions
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle   
 */
exports.trees_reload = function(req, res, next) {
	var api_key = req.query.api_key;
	var api_superuser_password = req.query.asup;
	//console.log('API SUPERUSER PASSWORD:' + config.conf.api_superuser_password);
	if(config.conf.api_superuser_password == api_superuser_password) {
	//utils.valid_api_key(api_key, res, function() {
		//clear the cache
		console.log('Clear the cache');
		ct_cache.clear();
		//refresh all dependent views
		query ("REFRESH MATERIALIZED VIEW chado.new_geno_view", [], function (err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			console.log('chado.new_geno_view refresh completed');
			query ("REFRESH MATERIALIZED VIEW chado.plusgeno_view", [], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				console.log('chado.plusgeno_view refresh completed');
				query ("REFRESH MATERIALIZED VIEW chado.new_pheno_view", [], function (err, rows, result) {
					if (err) {
						console.log(err);
						res.status(400);
						return;
					}	
					console.log('chado.new_pheno_view refresh completed');
					query ("REFRESH MATERIALIZED VIEW chado.ct_view", [], function (err, rows, result) {
						if (err) {
							console.log(err);
							res.status(400);
							return;
						}
						console.log('chado.ct_view refresh completed');
						//add new trees to table
						query ("SELECT * FROM chado.ct_view", [], function (err, rows, result) {
							if (err) {
								console.log(err);
								res.status(400);
								return;
							}
							console.log('chado.ct_view inserting new trees...');
							console.log('ROWS:' + rows.length);
							utils.insert_new_trees(res, rows);
							console.log('chado.ct_view finished inserting new trees');
							//refresh the view that houses all the trees and their respective data
							var wait_time = rows.length * 3; //rows by 3ms
							console.log('Waiting ' + (wait_time / 1000) + ' seconds before refreshing ct_trees_all_view ...');
							setTimeout(function() {
								console.log('Beginning refresh of ct_trees_all_view...');
								query ("REFRESH MATERIALIZED VIEW ct_trees_all_view", [], function (err, rows, result) {
									if (err) {
										console.log(err);
										res.status(400);
										return;
									}
									console.log('ct_trees_all_view refresh completed');
									console.log('Clear the cache');
									ct_cache.clear();								
								});	
							}, wait_time);
						});							
					});
				});
			});
		});
		/*
		//THIS HAS BEEN REMOVED IN FAVOUR OF NESTED IF ABOVE SO THAT THE REFRESHES GET EXECUTED IN A SPECIFIC ORDER.
		//add new trees to table
		query ("SELECT * FROM chado.ct_view", [], function (err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			utils.insert_new_trees(res, rows);
		});	
		//refresh the view that houses all the trees and their respective data
		query ("REFRESH MATERIALIZED VIEW ct_trees_all_view", [], function (err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
		});	
		*/
		
		console.log('trees_reload finished.');
		res.status(200).json("success - reload will continue to run in the background");
		return;
	//});
	}
	else {
		res.status(200).json("no asup was given");
	}
}

/**
 * Outputs status of reload - whether it is still performing refresh materialized views basically.
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle   
 */
exports.trees_reload_status = function(req, res, next) {
	var api_key = req.query.api_key;
	var api_superuser_password = req.query.asup;
	//console.log('API SUPERUSER PASSWORD:' + config.conf.api_superuser_password);
	if(config.conf.api_superuser_password == api_superuser_password) {
	//utils.valid_api_key(api_key, res, function() {
		//refresh the view that houses all the trees and their respective data
		query ("SELECT COUNT(pid) as ncount FROM (SELECT pid, now() - pg_stat_activity.query_start AS duration, query, state FROM pg_stat_activity WHERE query ILIKE 'REFRESH MATERIALIZED VIEW%') AS TABLE1;", [], function (err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			//console.log(rows);
			rows.forEach(row => {
				if(row.ncount > 0) {
					res.status(200).json("running");
					return;					
				}
				else {
					res.status(200).json("not running");
					return;						
				}
				//console.log('NCount: ' + row.ncount);
			});			
			//console.log('trees_reload_status finished.');

		});			
		
	//});
	}
	else {
		res.status(200).json("no asup was given");
	}	
}

/**
 * Removes source id = 0 trees which are the TreeGenes trees from the public.ct_trees table
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle   
 */
exports.remove_treegenes_trees = function(req, res, next) {
	var api_key = req.query.api_key;

	var api_superuser_password = req.query.asup;
	//console.log('API SUPERUSER PASSWORD:' + config.conf.api_superuser_password);
	if(config.conf.api_superuser_password == api_superuser_password) {

	//utils.valid_api_key(api_key, res, function() {
		console.log('Removing TreeGenes trees from the public.ct_trees table. Make sure to perform a reload to restore ct_trees.');
		query ("DELETE FROM public.ct_trees WHERE source_id = 0;", [], function (err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			res.status(200).json("success");
			return;	
		});
	//})
	}
	else {
		res.status(200).json("no asup was given");
	}	
}

/**
 * Clears the in memory cache for everything (complete cache clear)
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle   
 */
exports.cache_clear = function(req, res, next) {
	var api_key = req.query.api_key;

	var api_superuser_password = req.query.asup;
	//console.log('API SUPERUSER PASSWORD:' + config.conf.api_superuser_password);
	if(config.conf.api_superuser_password == api_superuser_password) {
	//utils.valid_api_key(api_key, res, function() {
		//clear the cache
		console.log('Clear the cache');
		ct_cache.clear();
		
		console.log('cache_clear finished.');
		res.status(200).json("success");
		return;
	//});
	}
	else {
		res.status(200).json("no asup was given");
	}	
}

exports.treesnap_consolidate_subkingdoms = async function(req, res, next) {
	var api_superuser_password = req.query.asup;
	if(config.conf.api_superuser_password == api_superuser_password) {
		setTimeout(function() {
			var pg_n_client = new pg_native_client();
			try {
				var species_without_organismprop_subkingdom_data = "";
				pg_n_client.connectSync('postgresql://' + config.conf.user + ':' + config.conf.password + '@' + config.conf.host + ':' + config.conf.port + '/' + config.conf.database + '');
				var rows = pg_n_client.querySync('SELECT DISTINCT(genus,species), genus, species FROM ct_trees WHERE subkingdom IS NULL AND source_id = 1;');
				if(rows.length > 0) {
					for(i=0; i<rows.length; i++) {
						genus = rows[i].genus;
						species = rows[i].species;
						species_parts = species.split(' ');
						species_name = species_parts[1];
						//console.log('RESULTS1:' + genus + ':' + species + ':' +  species_name);
						
						//This will find the subkingdom if one exists
						var rows2 = pg_n_client.querySync("SELECT * FROM chado.organism LEFT JOIN chado.organismprop ON chado.organism.organism_id = chado.organismprop.organism_id WHERE genus ILIKE '" + genus + "' AND organismprop.type_id = 35 LIMIT 1;");
						if(rows2.length > 0) {
							//If it reaches here, it's only one record found since we used LIMIT 1 above
							for(j=0; j<rows2.length; j++) {
								//perform the update
								var temp_organism_id = rows2[j].organism_id;
								var temp_subkingdom = rows2[j].value;
								var temp_genus = genus;
								var temp_species_name = species_name;
									 
								//console.log('RESULTS2:'  + temp_genus + ':' + temp_species_name + ':' + temp_subkingdom + ':' + temp_organism_id);								
								//console.log('Updating all records where subkingdom is null and species is:' + species + '. NEW SUBKINGDOM:' + temp_subkingdom);
								//pg_n_client.querySync("UPDATE ct_trees SET subkingdom = '" + temp_subkingdom + "' WHERE genus ILIKE '" + genus + "' AND species ILIKE '" + species + "' AND subkingdom IS NULL");
								
								console.log('Updating all records where subkingdom is null and genus is:' + genus + '. NEW SUBKINGDOM:' + temp_subkingdom);
								pg_n_client.querySync("UPDATE ct_trees SET subkingdom = '" + temp_subkingdom + "' WHERE genus ILIKE '" + genus + "' AND subkingdom IS NULL AND source_id=1;");
							}
						}
						else {
							console.log('WARNING: Could not find organism information to update treesnap tree for species: ' + species);
							genus_lowercase = genus.toLowerCase();
							if(genus_lowercase == 'pinus' || genus_lowercase == 'acer') {
								
							}
							else {
								species_without_organismprop_subkingdom_data = species_without_organismprop_subkingdom_data + species + "\n";
							}
						}
					}
				}
				pg_n_client.querySync("UPDATE ct_trees SET subkingdom = 'angiosperms' WHERE genus ILIKE 'acer' AND subkingdom IS NULL AND source_id=1;");
				pg_n_client.querySync("UPDATE ct_trees SET subkingdom = 'gymnosperms' WHERE genus ILIKE 'pinus' AND subkingdom IS NULL AND source_id=1;");
				console.log('Saving failed species to ' + __basedir + '/info_species_without_organismprop_subkingdom_data.txt');
				fs.writeFile(__basedir + '/info_species_without_organismprop_subkingdom_data.txt', species_without_organismprop_subkingdom_data, function (err) {
				  if (err) throw err;
				  console.log('Saved!');
				});
				species_without_organismprop_subkingdom_data = "";
				pg_n_client.end();
			}
			catch(err) {
				console.log(err);
			}
		
		},1000);
		res.status(200).json("success");
		return;		
	}
	else {
		res.status(200).json("no asup was given");
	}
}

exports.treesnap_consolidate_families = async function(req, res, next) {
	var api_superuser_password = req.query.asup;
	if(config.conf.api_superuser_password == api_superuser_password) {
		setTimeout(function() {
			var pg_n_client = new pg_native_client();
			try {
				//var species_without_organismprop_subkingdom_data = "";
				pg_n_client.connectSync('postgresql://' + config.conf.user + ':' + config.conf.password + '@' + config.conf.host + ':' + config.conf.port + '/' + config.conf.database + '');
				var rows = pg_n_client.querySync('SELECT DISTINCT(genus,species), genus, species FROM ct_trees WHERE family IS NULL AND source_id = 1;');
				if(rows.length > 0) {
					for(i=0; i<rows.length; i++) {
						genus = rows[i].genus;
						species = rows[i].species;
						species_parts = species.split(' ');
						species_name = species_parts[1];
						//console.log('RESULTS1:' + genus + ':' + species + ':' +  species_name);
						
						//This will find the subkingdom if one exists
						var rows2 = pg_n_client.querySync("SELECT * FROM chado.organism LEFT JOIN chado.organismprop ON chado.organism.organism_id = chado.organismprop.organism_id WHERE genus ILIKE '" + genus + "' AND organismprop.type_id = 10 LIMIT 1;");
						if(rows2.length > 0) {
							//If it reaches here, it's only one record found since we used LIMIT 1 above
							for(j=0; j<rows2.length; j++) {
								//perform the update
								var temp_organism_id = rows2[j].organism_id;
								var temp_family = rows2[j].value;
								var temp_genus = genus;
								var temp_species_name = species_name;
									 
								//console.log('RESULTS2:'  + temp_genus + ':' + temp_species_name + ':' + temp_subkingdom + ':' + temp_organism_id);								
								//console.log('Updating all records where subkingdom is null and species is:' + species + '. NEW SUBKINGDOM:' + temp_subkingdom);
								//pg_n_client.querySync("UPDATE ct_trees SET subkingdom = '" + temp_subkingdom + "' WHERE genus ILIKE '" + genus + "' AND species ILIKE '" + species + "' AND subkingdom IS NULL");
								
								console.log('Updating all records where family is null and genus is:' + genus + '. NEW Family:' + temp_family);
								pg_n_client.querySync("UPDATE ct_trees SET family = '" + temp_family + "' WHERE genus ILIKE '" + genus + "' AND family IS NULL AND source_id=1;");
							}
						}
						else {
							console.log('WARNING: Could not find organism information to update treesnap tree for species: ' + species);
							/*
							genus_lowercase = genus.toLowerCase();
							if(genus_lowercase == 'pinus' || genus_lowercase == 'acer') {
								
							}
							else {
								species_without_organismprop_subkingdom_data = species_without_organismprop_subkingdom_data + species + "\n";
							}
							*/
						}
					}
				}
				/*
				pg_n_client.querySync("UPDATE ct_trees SET subkingdom = 'angiosperms' WHERE genus ILIKE 'acer' AND subkingdom IS NULL AND source_id=1;");
				pg_n_client.querySync("UPDATE ct_trees SET subkingdom = 'gymnosperms' WHERE genus ILIKE 'pinus' AND subkingdom IS NULL AND source_id=1;");
				console.log('Saving failed species to ' + __basedir + '/info_species_without_organismprop_subkingdom_data.txt');
				fs.writeFile(__basedir + '/info_species_without_organismprop_subkingdom_data.txt', species_without_organismprop_subkingdom_data, function (err) {
				  if (err) throw err;
				  console.log('Saved!');
				});
				species_without_organismprop_subkingdom_data = "";
				*/
				pg_n_client.end();
			}
			catch(err) {
				console.log(err);
			}
		
		},1000);
		res.status(200).json("success");
		return;		
	}
	else {
		res.status(200).json("no asup was given");
	}
}

/**
 * Adds the new treesnap trees to db
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.reload_treesnap = function(req, res, next) {
	var api_superuser_password = req.query.asup;
	

	var api_key = req.query.api_key;
	
	//utils.valid_api_key(api_key, res, function() {
	if(config.conf.api_superuser_password == api_superuser_password) {	
		ct_cache.clear();		
		query ("SELECT uniquename FROM ct_trees WHERE source_id = 1 ORDER BY tree_num DESC LIMIT 1", [], function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			var uniquename = rows[0].uniquename;
			var treesnap_num = uniquename.split(".")[1];
			var page_num = Math.floor(Math.max(treesnap_num/100 - 6, 1));

			console.log('Performing Treesnap_add page no:' + page_num);
			utils.treesnap_add("https://treesnap.org/web-services/v1/observations?page=" + page_num);
			res.status(200).json("success");
			return;
		});	
	}
	//});
}

exports.force_reload_treesnap = function(req, res, next) {
	var api_superuser_password = req.query.asup;
	

	var api_key = req.query.api_key;
	
	//utils.valid_api_key(api_key, res, function() {
	if(config.conf.api_superuser_password == api_superuser_password) {	
		ct_cache.clear();		
		//var uniquename = rows[0].uniquename;
		//var treesnap_num = uniquename.split(".")[1];
		var page_num = 1;
		
		//console.log('Performing Treesnap_add page no:' + page_num);
		utils.treesnap_add("https://treesnap.org/web-services/v1/observations?page=" + page_num);
		res.status(200).json("success");
		return;
	}
	//});
}

/**
 * Returns a json object with the species, genus and family fields for all trees that we have in our database
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_get_fields = function(req, res, next) {
	var api_key = req.query.api_key;
	utils.valid_api_key(api_key, res, function() {
		var cached_val = ct_cache.get("trees_fields");	
		if (cached_val == null) {
			query ("SELECT DISTINCT ON(species) species, genus, family FROM ct_trees", [], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				var trees_fields = {"species": [], "genus": {}, "family": {}};
				for (var i = 0; i < rows.length; i++) {
					trees_fields["species"].push(rows[i]["species"]);

					if (rows[i]["genus"] != null && rows[i]["genus"].length > 0) {
						trees_fields["genus"][rows[i]["genus"]] = true;
					}
					if (rows[i]["family"] != null && rows[i]["family"].length > 0) {
						trees_fields["family"][rows[i]["family"]] = true;
					}
				}
				ct_cache.put("trees_fields", trees_fields, cache_time_24);
				res.status(200).json(trees_fields);
				return;
			});
		}	
		else {
			res.status(200).json(cached_val);
			return;
		}
	});
}

/**
 * Returns all the currently available marker types
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.markers_get_fields = function(req, res, next) {
	var api_key = req.query.api_key;
	
	utils.valid_api_key(api_key, res, function() {
		var cached_val = ct_cache.get("markers_fields");
		if (cached_val == null) {
			query ("SELECT * FROM geno_view_distinct ORDER BY marker_type", [], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				var output_arr = [];
				for (var i = 0; i < rows.length; i++) {
					output_arr.push(rows[i].marker_type);
					//res.status(200).json(rows);
				}
					
				ct_cache.put("marker_options", output_arr);
				res.status(200).json(output_arr);
				return;
			});
		}
		else {
			res.status(200).json(cached_val);
			return;
		}
	});
}

/**
 * Returns the basic information of a tree
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.tree_get_basic = function(req, res, next) {
	var tree_id = req.query.tree_id;
	var api_key = req.query.api_key;
	
	utils.valid_api_key(api_key, res, function() {
		var cached_val = ct_cache.get("tree_id=" + tree_id);
			if(cached_val == null) {
			query ("SELECT * FROM ct_trees WHERE uniquename = $1", [tree_id], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				ct_cache.put("tree_id=" + tree_id, rows[0], cache_time_24);
				res.status(200).json(rows[0]);
				return;
			});
		}
		else {
			res.status(200).json(cached_val);
			return;
		}	
	});
};


/**
 * Returns the phenotypes of the passed tree id
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.phenotypes_tree_get = function(req, res, next) {
	var tree_id = req.query.tree_id;
	var api_key = req.query.api_key;	
	
	utils.valid_api_key(api_key, res, function() {
		var key = tree_id + "_phenotype";
		var cached_val = ct_cache.get(key);
		if (cached_val == null) {
			query ("SELECT * FROM chado.new_pheno_view WHERE tree_acc = $1", [tree_id], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				else {
					ct_cache.put(key, rows, cache_time_24);
					res.status(200).json(rows);
					return;
				}
			});	
		}
		else {
			res.status(200).json(cached_val);
			return;
		}
	});
}

/**
 * Returns the markers of the passed tree id
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.markertypes_tree_get = function(req, res, next) {
	var tree_id = req.query.tree_id;
	var api_key = req.query.api_key;
	
	utils.valid_api_key(api_key, res, function() {	
		var key = tree_id + "_markertype";
		var cached_val = ct_cache.get(key);
		if (cached_val == null) {
			query ("SELECT DISTINCT ON(marker_type) * FROM chado.new_geno_view WHERE tree_acc = $1", [tree_id], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				else {
					ct_cache.put(key, rows, cache_time_24);
					res.status(200).json(rows);
					return;
				}
			});	
		}
		else {
			res.status(200).json(cached_val);
			return;
		}
	});
}

/**
 * Returns the publication information of a tree
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.publications_tree_get = function(req, res, next) {
	var tree_acc = req.query.tree_acc;
	var api_key = req.query.api_key;
		
	utils.valid_api_key(api_key, res, function() {
		var key = tree_acc + "_pub";
		var cached_val = ct_cache.get(key);
		if (cached_val == null) {
			query ("SELECT * FROM chado.plusgeno_view WHERE accession = $1", [tree_acc], function (err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				else {
					ct_cache.put(key, rows, cache_time_24);
					res.status(200).json(rows);
					return;
				}
			});	
		}
		else {
			res.status(200).json(cached_val);
			return;
		}
	});
}

/**
 * Returns the treesnap data and properties of a treesnap tree as an object
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.tree_get_treesnap = function(req, res, next) {
	var tree_id = req.query.tree_id.split(".")[1];
	var api_key = req.query.api_key;		

	utils.valid_api_key(api_key, res, function() {
		var key = "ts_"+ tree_id; 
		var cached_val = null;//ct_cache.get(key);

		if (cached_val == null) {
			var options = {
				uri: "https://treesnap.org/web-services/v1/observation/" + tree_id, 
				method: "GET",
				headers: {
					"Authorization": "Bearer " + process.env.TREESNAP_BEARER,
					"Accept": "application/json",
				},
			}
			var req_result = request(options, function(error, response, body) {
				if (error) {
					console.log(error);
					res.status(400);
					return;
				}

				if (response.statusCode == 200) {
					ct_cache.put(key, body, cache_time_24);
					res.status(200).json(JSON.parse(body).data);
					return;
				}	
				res.status(response.statusCode).json("status code: " + response.statusCode);
				return;
			});
		}
		else {
			res.status(200).json(val);
			return;
		}
	});
}

/**
 * Returns the total number of publications currently recorded
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.total_publications = function(req, res, next) {	
	query ("SELECT COUNT(*) AS count FROM (SELECT DISTINCT ON(accession) * FROM chado.plusgeno_view) p", [], function(err, rows, result) {
		if (err) {
			console.log(err);
			res.status(400);
			return;
		}
		res.status(200).json(rows[0]["count"]);
		return;
	});
}

/**
 * Returns the total number of species currently recorded
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.total_species = function(req, res, next) {
	query ("SELECT COUNT(*) AS count FROM (SELECT DISTINCT ON(species) * FROM ct_trees) t", [], function(err, rows, result) {
		if (err) {
			console.log(err);
			res.status(400);
			return;
		}
		res.status(200).json(rows[0]["count"]);
		return;
	});
}

/**
 * Returns the total number of trees currently recorded
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.total_trees = function(req, res, next) {
	query ("SELECT COUNT(*) AS count FROM (SELECT DISTINCT ON(uniquename) * FROM ct_trees) t", [], function(err, rows, result) {
		if (err) {
			console.log(err);
			res.status(400);
			return;
		}
		res.status(200).json(rows[0]["count"]);
		return;
	});
}

function build_tree_query_str(query_conditions, sources) {	
	var query_str = "SELECT DISTINCT ON(uniquename) * FROM ct_trees_all_view WHERE";
	//var query_str = "SELECT * FROM ct_trees_all_view WHERE";
	if (query_conditions.length > 0) {
		query_str = query_str + " " + query_conditions + " AND (";
	}
	else {
		query_str = "SELECT DISTINCT ON(uniquename) * FROM ct_trees WHERE (";
	}

	for (var s = 0; s < sources.length; s++) {
		if (s > 0) {
			query_str += " OR ";
		}
		query_str += "source_id = " + sources[s];
	}
	query_str += ")";
	return query_str;
}

/**
 * Receives a json object which outlines the queries desired by the user, and creates a sql query out of it after parsing
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_list_qbuilder = function(req, res, next) {
	var api_key = req.query.api_key;
	
	utils.valid_api_key(api_key, res, function() {
		var query_str = build_tree_query_str(utils.parse_json_query(req.body["query"], ""), req.body["active_sources"]);
		var cached_trees = ct_cache.get(query_str);
		console.log(query_str);
		if (cached_trees == null) {
			console.log('Returning trees from direct Postgresql');
			query (query_str, [], function(err, rows, result) {
				if (err) {
					console.log(err);
					res.status(400);
					return;
				}
				var tree_features = {"features":[], "center":[null, null]};
				var most_common = 0;
				var coords_to_freq = {};
				var freq_to_coords = {};
				var unq_species = {};
				var unq_pub = {};
				var tree_count = 0;
				for (var i = 0; i < rows.length; i++) {
					var curr_tree = {"type": "Feature"};
					curr_tree["properties"] = {"id": rows[i]["uniquename"], "icon_type": rows[i]["icon_type"]};
					curr_tree["geometry"] = {"type": "point", "coordinates": [rows[i]["longitude"], rows[i]["latitude"]]};
					if (rows[i]["accession"] != null && rows[i]["accession"].length > 0) {
						unq_pub[rows[i]["accession"]] = true;
					}						
					if (rows[i]["species"].length > 0) {
						unq_species[rows[i]["species"]] = true;	
					}
						
					var coords_key = rows[i]["longitude"] + "_" + rows[i]["latitude"];
					var coords_count = coords_to_freq[coords_key] == undefined ? 1 : coords_count + 1;
						
					coords_to_freq[coords_key] = coords_count;
					freq_to_coords[coords_count] = coords_key;
					if (coords_count > most_common) {
						most_common = coords_count;
					}
					tree_features["features"].push(curr_tree);
					tree_count++;
				}
				if (most_common > 0) {
					var coords_split = freq_to_coords[most_common].split("_");
					tree_features["center"] = [parseFloat(coords_split[0]), parseFloat(coords_split[1])];
				}
				tree_features["num_species"] = Object.keys(unq_species).length;
				tree_features["num_pubs"] = Object.keys(unq_pub).length;
				tree_features["num_trees"] = tree_count;

				ct_cache.put(query_str, tree_features, cache_time_24);
				
				res.status(200).json(tree_features);
				return;
			});
		}
		else {
			console.log('Retrieving trees from cache');
			res.status(200).json(cached_trees);
			return;
		}
	});
};

/**
 * Returns all the trees that are currently present in the db
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_list = function(req, res, next) {
 	var cached_trees = ct_cache.get("trees_list");
	if(cached_trees == null) {
		query ("SELECT * FROM ct_trees", [], function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			var treeFeatures = [];
			for (var i = 0; i < rows.length; i++) {
				var currTree = {"type": "Feature"};
				currTree["properties"] = {"id": rows[i]["uniquename"], "icon_type": rows[i]["icon_type"]};
				currTree["geometry"] = {"type": "point", "coordinates": [rows[i]["longitude"], rows[i]["latitude"]]};
				treeFeatures.push(currTree);
			}
			ct_cache.put("trees_list", treeFeatures, cache_time_24);
			res.status(200).json(treeFeatures);
			return;
		});
	}
	else {
		res.status(200).json(cached_trees);
		return;
	}	
};

function build_snp_query(trees_list) {
	var query_cond = "(";
	if (typeof(trees_list) == "object") {
		for (var i = 0; i < trees_list.length; i++) {
			query_cond += "$" + (i + 1);
			if (i < trees_list.length - 1) {
				query_cond += ", ";
			}
		}
	}
	else {
		query_cond += "$1";
		trees_list = [trees_list];
	}
	query_cond += ") ORDER BY tree_acc, marker_name";
	return {"query_str": "SELECT tree_acc, marker_name, description FROM chado.new_geno_view WHERE marker_type = 'SNP' AND tree_acc IN " + query_cond, "query_params": trees_list};
}

exports.trees_snps_missing = function(req, res, next) {
	var trees_list = req.body["trees"];
	var sql_query = build_snp_query(trees_list);
	
	//var cached_snp_missing = ct_cache.get(sql_query);
		
	//if (cached_snp_missing == null) {
		query(sql_query["query_str"], sql_query["query_params"], function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}		

			var snps_missing = {};
			var trees_snp_data = {};
			var markers_all = {};
			//transfer to util.js file as functions
			for (var i = 0; i < rows.length; i++) {
				var tree = rows[i]["tree_acc"];
				var marker = rows[i]["marker_name"];
				var marker_des = rows[i]["description"];

				markers_all[marker] = 0;
				if (trees_snp_data[tree] == undefined) {			
					trees_snp_data[tree] = {};
				}
				
				trees_snp_data[tree][marker] = marker_des;
			}	
			
			var tree_snp_missing = {};
			for (var tree_id in trees_snp_data) {
				var tree_missing_count = 0;
				for (var snp in markers_all) {
					var tree_snp_des = trees_snp_data[tree_id][snp];
					if (tree_snp_des == undefined) {	
						var missing_count = snps_missing[snp];
						snps_missing[snp] = missing_count == undefined ? 1 : missing_count + 1;	
						tree_missing_count++;
					}
					else {
						if (tree_snp_des == "NA" || tree_snp_des.length <= 2) {
							var missing_count = snps_missing[snp];
							snps_missing[snp] = missing_count == undefined ? 1 : missing_count + 1;	
							tree_missing_count++;
						}
					}
				}
				tree_snp_missing[tree_id] = (tree_missing_count/Object.keys(markers_all).length) * 100;
			}

			var num_trees = Object.keys(trees_snp_data).length;
			var snps_missing_freq = {};
			var freq_to_snp = {};
			for (var snp in snps_missing) {
				var missing_percent = (snps_missing[snp]/num_trees) * 100;
				snps_missing_freq[snp] = missing_percent;
				if (freq_to_snp[missing_percent] == undefined) {
					freq_to_snp[missing_percent] = [];
				}
				freq_to_snp[missing_percent].push(snp);
			}
			//ct_cache.put(sql_query, JSON.stringify({"snps_to_missing_freq": snps_missing_freq, "freq_to_snp": freq_to_snp, "tree_snp_missing": tree_snp_missing}), cache_time_24);
			res.status(200).json({"snps_to_missing_freq": snps_missing_freq, "freq_to_snp": freq_to_snp, "tree_snp_missing": tree_snp_missing});
			return;
		});	
	/*}
	else {
		res.status(200).json(JSON.parse(cached_snp_missing));
		return;
	}*/
	//res.status(200).json(sql_query);
}

/**
 * Returns all the snp markers a tree currently has
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_snp = function(req, res, next) {
	var trees_list = req.query.tree_id;	
	var sql_query = build_snp_query(trees_list);
	
	query(sql_query["query_str"], sql_query["query_params"], function(err, rows, result) {
		if (err) {
			console.log(err);
			res.status(400);
			return;
		}

		var marker_freq = {};
		var tree_snp_data = {};
		var markers_available = {};
		for (var i = 0; i < rows.length; i++) {
			var tree = rows[i]["tree_acc"];
			var marker = rows[i]["marker_name"];
			var marker_des = rows[i]["description"];
			var is_homo = false;

			if (marker_des.length > 2 && marker_des.charAt(0) == marker_des.charAt(2)) {
				is_homo = true;
			}

			if (tree_snp_data[tree] == undefined) {
				tree_snp_data[tree] = {};
			}		
			tree_snp_data[tree][marker] = marker_des;
			
			//var marker_count = markers_available[marker];
			markers_available[marker] = 0;//marker_count == undefined ? 1 : marker_count + 1;
			
			if (marker_freq[marker] == undefined) {
				marker_freq[marker] = {};
				marker_freq[marker]["max"] = 0;
				marker_freq[marker]["homo_ref"] = null;
			}
			
			var marker_des_count = marker_freq[marker][marker_des];
			marker_freq[marker][marker_des] = marker_des_count == undefined ? 1 : marker_des_count + 1;
			if (is_homo && marker_freq[marker][marker_des] > marker_freq[marker]["max"]) {
				marker_freq[marker]["max"] = marker_freq[marker][marker_des];
				marker_freq[marker]["homo_ref"] = marker_des;
			}
		}

		var sorted_markers = Object.keys(markers_available).sort();
		
		var snp_table = [["NAME"]];	
		var missing_freq = {};
		snp_table[0] = snp_table[0].concat(sorted_markers);

		for (var tree_id in tree_snp_data) {
			var snp_table_row = [tree_id];	
			for (var snp_idx in sorted_markers) {
				var snp = sorted_markers[snp_idx];
				if (tree_snp_data[tree_id][snp] != undefined) {
					var marker_val = 0;
					var marker_des = tree_snp_data[tree_id][snp];
					if (marker_des.length > 2){
						if (marker_des.charAt(0) != marker_des.charAt(2)) {
							marker_val = 1;
						}
						else if (marker_freq[snp]["homo_ref"] != marker_des) {
							marker_val = 2;
						}
					}
					else {
						marker_val = "NaN";
						var missing_count = missing_freq[snp];
						missing_freq[snp] = missing_count == undefined ? 1 : missing_count + 1;
					}
					snp_table_row.push(marker_val);
				}
				else {
					var missing_count = missing_freq[snp];
					missing_freq[snp] = missing_count == undefined ? 1 : missing_count + 1;
					snp_table_row.push("NaN");
				}
			}
			snp_table.push(snp_table_row);
		}

		var snp_percent_missing = {};
		for (var snp in missing_freq) {
			snp_percent_missing[snp] = missing_freq[snp]/Object.keys(tree_snp_data).length;
		}
		res.status(200).json(snp_percent_missing);
		return;
	});
};


/**
 * Returns the associated publications given search input of year and other publication properties
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
/*
exports.search_publications = function(req, res, next) {
	var year = req.params.year;
	var search_param = Object.keys(req.query);
	
	var cached_publications = ct_cache.get(year + "_" + search_param + "_pubs");
	
	var base_query = "SELECT DISTINCT ON(accession) * FROM ct_pub_view";
	var matching_vals = [];	

	if (!isNaN(year)) {
		base_query += " WHERE pyear = $1";
		matching_vals.push("" + year);

		if (search_param.length > 0) {
			base_query += " AND " + search_param + " = $2";
			matching_vals.push(req.query[search_param]);
		}
		
	} 
	else if (search_param.length > 0) {
		base_query += " WHERE " + search_param + " = $1";
		matching_vals.push(req.query[search_param]);
	}	
	
	if (cached_publications == null) {
		query (base_query, matching_vals, function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			res.status(200).json(rows);
			return;
		});
	}
}
*/

/**
 * Returns the trees of a given phenotype or combination phenotypes
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_phenotype = function(req, res, next) {
	var base_query = "SELECT tree_acc FROM chado.new_pheno_view WHERE";
	var query_args = req.query;
	var matching = [];
	var idx = 1;
	var k = "";
	if (query_args.hasOwnProperty("pheno")) {
		var phenos = query_args.pheno;
		if (typeof(phenos) == "object") {	
			var obj_keys = Object.keys(phenos).sort(function(a,b){return (phenos[a]).localeCompare(phenos[b])});
			for (var i = 0; i < obj_keys.length; i++) {
				base_query += " phenotype_name = $" + idx;// + publications[pub_keys_sorted[i]] + "%"";
				if (i < obj_keys.length - 1) {
					base_query += " OR";
				}
				k += phenos[obj_keys[i]];
				matching.push(phenos[obj_keys[i]]);
				idx++;
			}
		}
		else {
			matching.push(phenos);
			k += phenos;
			base_query += " phenotype_name = " + "$1";
			idx++;
		}
	}
	if (query_args.hasOwnProperty("pato")) {
		var patos = query_args.pato;	
		if (matching.length > 0) {
			base_query += " OR";
		}
		if (typeof(patos) == "object") {	
			var obj_keys = Object.keys(patos).sort(function(a,b){return (patos[a]).localeCompare(patos[b])});
			for (var i = 0; i < obj_keys.length; i++) {
				base_query += " pato_name = $" + idx;// + publications[pub_keys_sorted[i]] + "%"";
				if (i < obj_keys.length - 1) {
					base_query += " OR";
				}
				k += patos[obj_keys[i]];
				matching.push(patos[obj_keys[i]]);
				idx++;
			}
		}
		else {
			matching.push(patos);
			k += patos;
			base_query += " pato_name = " + " $" + idx;
			idx++;
		}
	}
	if (query_args.hasOwnProperty("po")) {	
		var pos = query_args.po;	
		if (matching.length > 0) {
			base_query += " OR";
		}
		if (typeof(pos) == "object") {	
			var obj_keys = Object.keys(pos).sort(function(a,b){return (pos[a]).localeCompare(pos[b])});
			for (var i = 0; i < obj_keys.length; i++) {
				base_query += " po_name = $" + idx;// + publications[pub_keys_sorted[i]] + "%"";
				if (i < obj_keys.length - 1) {
					base_query += " OR";
				}
				k += pos[obj_keys[i]];
				matching.push(pos[obj_keys[i]]);
				idx++;
			}
		}
		else {
			matching.push(pos);
			k += pos;
			base_query += " po_name = " + " $" + idx;
			idx++;
		}
	}
	
	var cached_pheno_trees = ct_cache.get(k + "_trees");
	if (cached_pheno_trees == null) {
		query (base_query, matching, function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}

			res.status(200).json(rows);
			return;
		});
	}
	else {
		res.status(200).json(cached_pheno_trees);
		return;
	}
}

/**
 * Returns the trees given a marker type or combination of marker types
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_genotype = function(req, res, next) {
	var query_props = utils.parse_uni_query(req.query.marker_type, false, "marker_type");
	query_props["query_str"] = "SELECT tree_acc FROM ct_geno_view WHERE" + query_props["query_str"];
	
	var cached_marker_trees = ct_cache.get(query_props["key"] + "_trees");
	if (cached_marker_trees == null) {
		query (query_props["query_str"], query_props["query_vals"], function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}

			res.status(200).json(rows);
			return;
		});
	}
	else {
		res.status(200).json(cached_marker_trees);
		return;
	}
}

/**
 * Returns the trees given a publication id or multiple publication ids
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_publication = function(req, res, next) {
	var query_props = utils.parse_uni_query(req.query.pub_id, true, "uniquename");
	query_props["query_str"] = "SELECT * FROM ct_trees WHERE" + query_props["query_str"];
	//res.json(query_props);
	
	var cached_pub_list = ct_cache.get(query_props["key"] + "_trees");

	if (cached_pub_list == null) {
		get_tree_ids(res, query_props);
	}
	else {	
		res.status(200).json(cached_pub_list);
		return;
	}
}

/**
 * Returns the trees given a single family name or multiple family name joined by OR conditions
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_family = function(req, res, next) {		
	var query_props = utils.parse_uni_query(req.query.family_name, false, "family");	
	query_props["query_str"] = "SELECT * FROM ct_trees WHERE" + query_props["query_str"];
	var cached_family_list = ct_cache.get(query_props["key"] + "_trees");

	if (cached_family_list == null) {
		get_tree_ids(res, query_props);
	}
	else {
		res.status(200).json(cached_family_list);
		return;
	}
}


/**
 * Returns the trees given a single genus name or multiple genus name joined by OR conditions
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_genus = function(req, res, next) {
	var query_props = utils.parse_uni_query(req.query.genus_name, false, "genus");	
	query_props["query_str"] = "SELECT * FROM ct_trees WHERE" + query_props["query_str"];
	var cached_genus_list = ct_cache.get(query_props["key"] + "_trees");

	if (cached_genus_list == null) {
		get_tree_ids(res, query_props);
	}
	else {
		res.status(200).json(cached_genus_list);
		return;
	}
}

/**
 * Returns the trees given a single genus name or multiple species name joined by OR conditions
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_species = function(req, res, next) {
	var query_props = utils.parse_uni_query(req.query.species_name, false, "species");
	query_props["query_str"] = "SELECT * FROM ct_trees WHERE" + query_props["query_str"];

	var cached_species_list = ct_cache.get(query_props["key"] + "_trees");
	if (cached_species_list == null) {
		get_tree_ids(res, query_props);
	}
	else {
		res.status(200).json(cached_species_list);
		return;
	}
}

/**
 * Returns the trees given any number of source combinations from {0, 1, 2}
 * @param {object} request - Request object
 * @param {object} response - Response object
 * @param {function} next - Next middleware function in the application"s request-response cycle
 */
exports.trees_by_source = function(req, res, next) {
	var sources = req.query.source_id;
	var sources_idx = Object.keys(sources);
	var sources_arr = [];
	var placeholder = sources[0];	

	for (var i = 0; i < 3; i++) {
		if (i < sources_idx.length) {
			sources_arr.push(sources[i]);
		}
		else {
			sources_arr.push(placeholder);
		}
	}

	var cached_trees = ct_cache.get(sources_arr.join("_") + "_trees_list");
	if (cached_trees == null) {
		query ("SELECT * FROM ct_trees WHERE source_id = $1 OR source_id = $2 OR source_id = $3", sources_arr, function(err, rows, result) {
			if (err) {
				console.log(err);
				res.status(400);
				return;
			}
			var treeFeatures = []; 
			for (var i = 0; i < rows.length; i++) {
				var currTree = {"type": "Feature"};
				currTree["properties"] = {"id": rows[i]["uniquename"], "icon": 0};
				currTree["geometry"] = {"type": "point", "coordinates": [rows[i]["longitude"], rows[i]["latitude"]]};
				treeFeatures.push(currTree);
			}
			ct_cache.put(sources_arr.join("_") + "_trees_list", treeFeatures, 60*60*1000);
			res.status(200).json(treeFeatures);
			return;
		});
	}
	else {
		res.status(200).json(cached_trees);
		return;
	}
};
