'use strict';


const conf = {
  host: '127.0.0.1', //process.env.DB_HOST,
  port: 5432, //process.env.DB_PORT,
  database: 'drupal', //process.env.DB_NAME,
  
  user: 'drupal', //process.env.DB_USER,
  password: 'password', //process.env.DB_PASS,
  
  api_superuser_password: 'changeme',
  
  // maximum number of clients the pool should contain
  // by default this is set to 10
  max: 15,
  // number of milliseconds a client must sit idle in the pool and not be checked out
  // before it is disconnected from the backend and discarded
  // default is 10000 (10 seconds) - set to 0 to disable auto-disconnection of idle clients
  idleTimeoutMillis: 30000,
  // number of milliseconds to wait before timing out when connecting a new client
  // by default this is 0 which means no timeout
  connectionTimeoutMillis: 2000,
};

exports.conf = conf;
