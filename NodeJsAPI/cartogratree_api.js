var express = require("express");
var responseTime = require("response-time");
var bodyParser = require("body-parser");
var cors = require("cors")
var app = express();
var port = process.env.PORT || 8088;

var dotenv = require("dotenv")

const result = dotenv.config();

global.__basedir = __dirname;

if (result.error) {
  throw result.error;
}
console.log('FROM DOTENV:');
console.log(result.parsed);

var logger = function(req, res, next) {
	console.log(req.url);
	next(); // Passing the request to the next handler in the stack.
}

app.use(logger);
app.use(cors());
app.use(responseTime());
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false}));

app.set("json spaces", 0);

var routes = require("./routes");	// import routes
routes(app);						// register routes

app.listen(port, "0.0.0.0");

app.use(function(req, res, next) {
	next(createError(404));
});
// address unhandled urls
app.use(function(err, req, res, next) {
	res.locals.message = err.message;
	
	res.status(err.status || 500).send({url: req.originalUrl + " not found"});
});

console.log("CartograTree API server started on: " + port);
