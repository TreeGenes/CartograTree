"use strict";
$(function () {

	//constant variables
	const bottomMostLayer = "soils-layer";
	const datasetLayerId = "tree-points";
	const datasetSourceId = "trees";
	const datasetClusterId = "tree-cluster";
	const clusterProps = [[0, "#6082b6"], [250,"#738678"], [1000, "#645452"]];
	const datasetKey = {"treegenes": 0, "treesnap": 1, "datadryad": 2};
	const defaultTreeImgs = ["https://via.placeholder.com/150/e7e7e7/000000/?text=Image%201", "https://via.placeholder.com/150/e7e7e7/000000/?text=Image%202", "https://via.placeholder.com/150/e7e7e7/000000/?text=Image%203"];	
	
	var treeImgsStore = {};
	var treeDataStore = {};
	var treesnapMetaCodes =	{
            "ashSpecies": "Species",
            "seedsBinary": "Seeds",
            "flowersBinary": "Flowers",
            "emeraldAshBorer": "Ash Borer",
            "woollyAdesCoverage": "Woolly Adelgids",
            "chestnutBlightSigns": "Chestnut Blight",
            "acorns": "Acorns",
            "cones": "Cones",
            "heightFirstBranch": "Height of First Branch",
            "oakHealthProblems": "Health Problems",
            "diameterNumeric": "Tree Diameter",
            "crownHealth": "Crown Health",
            "crownClassification": "Crown Classification",
            "otherLabel": "Tree Type",
            "locationCharacteristics": "Habitat",
            "nearbyTrees": "Trees Nearby",
            "nearByHemlock": "Nearby Hemlocks",
            "treated": "Treated",
            "partOfStudy": "Study",
            "heightNumeric": "Tree Height",
            "burrs": "Nuts/burrs",
            "catkins": "Catkins",
            "comment": "Comment",
            "diameterNumeric_confidence": "Diameter Confidence",
            "heightFirstBranch_confidence": "Height of First Branch Confidence",
            "numberRootSprouts": "Number of Root Sprouts",
            "numberRootSprouts_confidence": "Number of Root Sprouts Confidence",
            "heightNumeric_confidence": "Tree Height Confidence",
            "torreyaFungalBlight": "Fungal Blight",
            "conesMaleFemale": "Cones",
            "deerRub": "Deer Rub",
            "madroneDisease": "Disease",
            "crownAssessment": "Tree Crown Assessment",
            "standDiversity": "Stand Diversity"
	};
	var activeDatasets = {0: false, 1: false, 2: false};
	var activeTrees = {};
	var currentSession = Drupal.settings.session.session_id;
	var filterQuery = {};
	var clickedTrees = [];
	var prevSessionFilters = {};
	var mapState, layersList;
	var currMarker = null;

	var chartProperties = (function() {
		// set the dimensions and margins of the graph
		var margin = {top: 10, right: 30, bottom: 50, left: 40};
		var width = 460 - margin.left - margin.right;
		var height = 400 - margin.top - margin.bottom;

		function getHeight() {
			return height;
		}

		function getWidth() {
			return width;
		}	

		function getMargin() {
			return margin;
		}

		return {
			//chartSVG: svg,
			chartHeight: height,
			chartWidth: width,
			chartMargin: margin,
		}
	})();

	/**************************************************
 
	* MAPBOX INITIALIZATION *
 	
	**************************************************/
	mapboxgl.accessToken = Drupal.settings.mapbox_token;
	//properties of map on load 
	var map = new mapboxgl.Map({
		container: "map",
		style: "mapbox://styles/snkb/cjrgce15209bi2spi9f2ddvch",
		center: [-90, 40],
		scrollWheelZoom: true,
		dragPan: true,
		keyboard: true,
		zoom: 4,
		pitch: 0,
		bearing: 0,
		minZoom: 1,
		maxZoom: 15,
	});

	//toastr notifications options
	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": true,
		"progressBar": false,	
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
		
	/**
	 * A class that represents the environmental layers of the map
	 * @class
	*/
	class Layer {
		/**
		 * The constructor for the Layer class
		 * @param {string} layerId - The id of the layer
		 * @param {object} legend - The legend associated with the layer
		 * @param {integer} layerHost - Layers can come from mapbox or geoserver, 0 for geoserver 1 for mapbox
		 * @param {float} opacity - The opacity of the layer, 0-1
		 * @param {boolean} canQuery - Sets whether a layer can be queried for values and has variables
		*/
		constructor(layerId, legend, layerHost, opacity, canQuery) {
			this.id = layerId;
			this.sourceId = layerId;
			this.legend = legend;
			this.queryable = canQuery;
			this.opacity = opacity;
			this.active = false;
			this.host = layerHost;
			this.priority = 0;
			this.sourceLoaded = true;
			if (layerHost == 0) {
				this.sourceLoaded = false;
			}
		}
		
		/**
		 * The layers are ordered on the map, low priority number = higher priority = will be on top of other layers and most visible
		 * When a new layer is added it"s added to the top of the layers stack
		 * @param {integer} newPriority - representing the priority of the current layer relative to other layers 	 
		*/
		updatePriority(newPriority) {
			this.priority = newPriority;
		}

		/**
		 * Updates the opacity slider and its value when opacity changes
		*/	
		updateOpacity() {
			var layerNum = this.id.split("_")[2];
			$("#slider-" + layerNum + "-" + this.host).val(this.opacity*100);
			$("#opacity-value-" + layerNum).text(this.opacity*100 + "%");
		}
	}

	/**
	 * A class for the geoserver layers extends from the Layer class
	 * @class
	*/	
	class GeoserverLayer extends Layer {
		/**
		 * The constructor for GeoserverLayer class, it calls the parent class to create a Layer object
		 * @param {string} layerId - The id of the layer
		 * @param {object} legend - The legend associated with the layer
		 * @param {float} opacity - The opacity of the layer, 0-1
		 * @param {string} sourceType - What kind of layer is being loaded, can be raster or vector
		 * @param {boolean} queryable - Sets whether a layer can be queried for values and has variables
		*/
		constructor(layerId, legend, opacity, sourceType = "raster", queryable = true) {
			super(layerId, legend, 0, opacity, queryable);
			this.tileSize = 256;
			this.sourceType = sourceType;
			//this.leg = new Legend(Drupal.settings.layers[layerId].name);
		}
	
		/**
		 * Get the source url for geoserver layer 
		 * @return {string} source - The source of the geoserver layer 
		*/	
		getSource() {
			console.log(this.id);
			console.log(Drupal.settings.layers);
			var source = Drupal.settings.cartogratree.gis + "?service=WMS&version=1.1.0&request=GetMap&layers=";
			source += Drupal.settings.layers[this.id].name + "&styles=&transparent=true&bbox={bbox-epsg-3857}&width=256&height=256&srs=EPSG:3857&format=image/png";
			return source;
		}

		/**
		 * Adds the geoserver layer to the map and places it below the datasets
		*/	
		addLayer() {
			map.addLayer({
				"id": this.id,
				"type": this.sourceType,
				"source": this.sourceId,
				"paint": {
					"raster-opacity": this.opacity
				}
			}, bottomMostLayer);
		}

		/**
		 * Activates a geoserver layer, so it can be viewed and interacted by the user, it does this by updating the opacity and making it visible
		*/	
		activateLayer() {
			map.setPaintProperty(this.id, "raster-opacity", this.opacity);
			this.active = true;
			this.updateOpacity();
			this.legend.render();
		}

		/**
		 * Deactivates a geoserver layer and makes it invisible to the user
		*/	
		deactivateLayer() {
			map.setPaintProperty(this.id, "raster-opacity", 0);
			this.legend.remove();
			this.active = false;
		}

		/**
		 * Adds the source of the geoserver layer to the map
		*/	
		addSource() {
			map.addSource(this.id, {
				"type": this.sourceType,
				"tiles": [this.getSource()],
				"tileSize": this.tileSize
			});
			this.sourceLoaded = true;
		}

		/**
		 * Changes the opacity of the geoserver layer	
		 * @param {float} newOpacity - a value from 0 to 1
		*/	
		changeOpacity(newOpacity) {
			if (this.host == 0) {
				map.setPaintProperty(this.id, "raster-opacity", newOpacity);
			}
		}
	}

	/**
	 * A class for the Mapbox layers, extends from general Layer class
	 * @class
	*/	
	class MapboxLayer extends Layer {
		/**
		 * The constructor of the MapboxLayer which creates an instance of the Layer class 
		 * @param {string} layerId - The id of the layer
		 * @param {object} legend - The legend associated with the layer
		 * @param {float} opacity - The opacity of the layer, 0-1
		 * @param {array} subLayers - Environmental layers composited into base map typically composed of multiple parts, will contain an array of strings
		 * @param {boolean} queryable - Sets whether a layer can be queried for values and has variables
		*/
		constructor(layerId, legend, opacity, subLayers, queryable = true) {
			super(layerId, legend, 1, opacity, true);
			this.subLayers = subLayers;
		}

		/**
		 * Adds the mapbox layer and its sublayers to the map by making them visible
		*/	
		addLayer() {
			for(var i = 0; i < this.subLayers.length; i++) {
				map.setLayoutProperty(this.subLayers[i], "visibility", "visible");
			}
		}

		/**
		 * Activates a mapbox layer making it visible to the user and can be interacted with
		 * @param {float} opacity - the current opacity of the layer
		*/	
		activateLayer(opacity = this.opacity) {
			this.changeOpacity(opacity);
			this.updateOpacity();
			this.active = true;
			this.legend.render();
		}

		/**
		 * Deactivates a layer by chaning its opacity to 0 rendering it invisible
		*/	
		deactivateLayer() {	
			this.changeOpacity(0);
			this.legend.remove();
			this.active = false;
		}

		/**
		 * Changes the opacity of the layer and its sublayers
		 * @param {float} newOpacity - the new opacity of the layer
		*/	
		changeOpacity(newOpacity) {
			for (var i = 0; i < this.subLayers.length; i++) {
				var paintInfo = map.getLayer(this.subLayers[i]).paint;
				for (var k in paintInfo._values) {
					if (k.indexOf("opacity") !== -1){
						map.setPaintProperty(this.subLayers[i], k, newOpacity);
					}
				}
			}
		}
	}

	//TODO: Currently this only handles the geoserver layers, also need to handle for the mapbox case
	/**
	 * A class for the legend of an environmental layer
	 * @class
	*/	
	class Legend {
		/*
		 * Constructor for Legend class
		 * @param {String} title - title of legend
		*/ 
		constructor(title) {
			console.log(title);
			this.width = 300;
			this.height = 125;
			var layersToColors = {
				"Aridity": [["rgb(255,252,252)", "rgb(248,135,135)", "rgb(244,66,66)"], [34880, 0]],
				"Canopy": [["rgb(208,228,235)", "rgb(171,221,164)", "rgb(255,255,191)", "rgb(215,25,28)"], [24,0]],
				"Precipitation": [["rgb(255,255,217)", "rgb(237,248,177)", "rgb(65,182,196)", "rgb(8,29,88)"],[908,0]],
				"SolRad": [["rgb(255,255,250)", "rgb(255,255,170)"], [18.3,0]],
				"PET": [["rgb(242,255,250)", "rgb(120,248,203)", "rgb(66,244,182)"], [296,0]],
				"Solar radiation": [["rgb(255,246,102)", "rgb(255,240,0)", "rgb(255,0,0)"], [50000,0]],
				"temperature": [["rgb(0,0,255)", "rgb(203,203,51)", "rgb(255,255,0)", "rgb(255,187,0)", "rgb(255,0,0)"], [28,-55]],
				"Tree Cover": [["rgb(255,255,255)", "rgb(154,205,154)", "rgb(0,128,0)"], [100,0]],
				"Water vapor pressure": [["rgb(255,255,255)", "rgb(238,255,255)", "rgb(0,238,255)", "rgb(0,128,255)", "rgb(0,24,255)"], [3.6,0]],
				"Wind speed": [["rgb(204,204,204)", "rgb(153,153,153)","rgb(102,102,102)","rgb(0,0,0)"],[40,0]],
				"Soil": null,
				"Sites": null,	
				"Domain": null,
				"Sampling": null,
			}
			var labels = {
				"Aridity": "Aridity (Index)",
				"Canopy": "Canopy Height",
				"Precipitation": "Precipitation (mm)",
				"Water vapor pressure": "Vapor Pressure (kPA)",
				"temperature": "Temperature (C)",
				"SolRad": "Solar Radiation (kJ m^-2 day^-1)",
				"Solar radiation": "Solar Radiation (kJ m^-2 day^-1)",
				"Wind speed": "Wind Speed (m s^-1)",
				"PET": "PET (mm day^-1) ",
				"Tree Cover": "Tree Cover",
				"Soil": null,
				"Sites": null,
				"Domain": null,
				"Sampling": null,
			}

			//determines range of colors for legend based on title
			this.colors = null;
			if (title != null) {
				for (var layerType in layersToColors) {
					if (title.toLowerCase().indexOf(layerType.toLowerCase()) >= 0) {
						this.colors = layersToColors[layerType];
						this.title = labels[layerType];
						break;		
					}
				}
			}
		}

		/*
		 * Removes the legend
		*/ 	
		remove() {
			$("#legend").html("");
			d3.select("svg").remove();
		}

		/*
		 * Renders the legend on the map
		 * Creates a D3js svg object
		*/ 
		render() {
			if (this.colors != null) {
				var colors = this.colors[0];
				var valRange = this.colors[1];
	
				$("#legend").html("");
				var svg = d3.select("#legend")
				  .append("svg")
				  .attr("width", this.width)
				  .attr("height", this.height);

				var grad = svg.append("defs")
				  .append("linearGradient")
				  .attr("id", "grad")
				  .attr("x1", "0%")
				  .attr("x2", "100%")
				  .attr("y1", "0%")
				  .attr("y2", "0%");

				grad.selectAll("stop")
				  .data(colors)
				  .enter()
				  .append("stop")
				  .style("stop-color", function(d){ return d; })
				  .attr("offset", function(d,i){
					return 100 * (i / (colors.length - 1)) + "%";
				  });

				svg.append("rect")
				  .attr("x", 50)
				  .attr("y", 30)
				  .attr("width", 200)
				  .attr("height", 30)
				  .style("fill", "url(#grad)");

				var y = d3.scaleLinear()
				  .range([200, 0])
				  .domain(valRange);
				
				var yAxis = d3.axisBottom()
				  .scale(y)
				  .tickValues(valRange);
				
				svg.append("text")
        			.attr("x", (this.width / 2))             
       		 		.attr("y", 15)
        			.attr("text-anchor", "middle")  
        			.style("font-size", "16px") 
        			.style("text-decoration", "underline")  
        			.text(this.title);
				
				svg.append("g")
				  .attr("class", "y axis")
				  .attr("transform", "translate(50,60)")
				  .call(yAxis)
				  .append("text")
				  .attr("transform", "rotate(-90)")
				  .attr("y", -30)
				  .attr("x", 50)
				  .attr("dy", "0")
				  .attr("dx", "0")
				  .attr("fill", "#000")
			}
			else {
				$("#legend").html("");
			}
		}
	}
	
	/**
	 * A class for the layers list which will keep track of all the layer on the map
	 * @class
	*/	
	class LayersList {
		/**
		 * The constructor for LayersList
		 * @param {hashtable} loadedFromSession - takes as an argument all the layers active from a previous session
		*/	
		constructor(loadedFromSession = {}) {
			this.neon = ["neon-fieldsites", "neon-plots-poly", "neon-plots-circular"];
			
			this.mapboxLayers = {
				"cartogratree_layer_187": ["neon-fieldsites"], 
				"cartogratree_layer_188": ["neon-domains-fill", "neon-domains-text"],
				"cartogratree_layer_189": ["neon-plots-poly", "neon-plots-circular"],
				"cartogratree_layer_4": ["soils-layer", "soils-layer-border"],
			};

			this.excludedLayers = ["cartogratree_layer_40", "cartogratree_layer_25", "cartogratree_layer_26", "cartogratree_layer_27", "cartogratree_layer_28"];	
			this.sessionLoadedLayers = loadedFromSession;
			//contains a key/value pair of layer ids and layer objects associated with each loaded layer
			this.layers = {};
			this.layersStack = [];
		}		

		/**
		 * Gets the layer object based on id specified
		 * @param {string} layerId - the id of the layer
		 * @return {object} or null if the layer has not been added
		*/	
		getLayer(layerId) {
			if (this.layers.hasOwnProperty(layerId)) {
				return this.layers[layerId];
			}
			return null;
		}

		/**
		 * Checks if a layer is active
		 * @param {string} layerId - the id of the layer
		 * @return {boolean} if the layer is added and is active then returns true, false otherwise
		*/	
		isActive(layerId) {
			return this.getLayer(layerId) != null && this.layers[layerId].active == true;
		}

		/**
		 * Adds the layer to the map and to the layers list
		 * @param {object} layer - the layer object to be added
		*/	
		addToMap(layer) {
			if (!layer.sourceLoaded) {
				layer.addSource();
			}
			layer.addLayer();
			this.layers[layer.id] = layer;
		}

		/**
		 * Changes the opacity of the layer
		 * @param {string} layerId - the id of the layer
		 * @param {float} newOpacity - the opacity to be set
		*/	
		changeOpacity(layerId, newOpacity) {
			var layer = this.getLayer(layerId);
			if (this.isActive(layerId)) {
				layer.changeOpacity(newOpacity);
				layer.opacity = newOpacity;
				layer.updateOpacity();
			}
		}

		/**
		 * Activates the layer specified and puts it at the top of the stack
		 * @param {string} layerId - id of the layer
		*/	
		activateLayer(layerId) {
			var layer = this.getLayer(layerId);
			if (!this.isActive(layerId)) {
				layer.activateLayer();
				this.layersStack.push(layer.id);
				$("#num-layers").text(this.layersStack.length);
			}	
		}

		/**
		 * deactivates the layer specified and removes it from the stack
		 * @param {string} layerId - id of the layer
		*/	
		deactivateLayer(layerId) {
			var layer = this.getLayer(layerId);
			if (this.isActive(layerId)) {
				layer.deactivateLayer();
				this.layersStack.splice(layer.id, 1);
				$("#num-layers").text(this.layersStack.length);
			}	
		}

		/**
		 * gets the active layers on the map
		 * @return {array} activeLayers - an array containing the ids of the active environmental layers
		*/	
		getActiveLayers() {
			var activeLayers = [];	
			for (var layerId in this.layers) {
				if (this.layers[layerId].active) {
					activeLayers.push(layerId);
				}
			}
			return activeLayers;
		}

		/**
		 * Deactivate all layers and update number of layers
		*/ 
		resetLayers() {
			var activeLayers = this.getActiveLayers();
			for (var i = 0; i < activeLayers.length; i++) {
				this.deactivateLayer(activeLayers[i]);
			}
			$("#num-layers").text(0);
		}

		/**
		 * Get all the layers that will be saved for this current session, layers that have been loaded will be saved
		 * @return {hashtable/js object} layersToSave - Will contain a series of key/value pairs for each layer and its properties
		 * layersToSave = {<layer1_id> : {<opacity>:<opacity_val>, <active>:<true/false>, <host>:<0/1>}, <layer2_id> ... }
		*/	
		getLayersToSave() {
			let layersToSave = {};
			for (var layerId in this.layers) {
				let layerObj = this.layers[layerId];
				layersToSave[layerId] = {
					"opacity": layerObj.opacity, 
					"active": layerObj.active, 
					"host": layerObj.host
				};
			}
			return layersToSave;
		}

		/**
		 * loads all the layers that were loaded from the previous session and deactivates/activates them based on the map state at the time of save
		*/	
		initPreloaded() {			
			for (var layer in this.sessionLoadedLayers) {	
				if (this.sessionLoadedLayers.hasOwnProperty(layer) && this.excludedLayers.indexOf(layer) == -1) {
					let layerObj;
					let host = this.sessionLoadedLayers[layer].host;
					if (host == 0) { //geoserver hosted layers
						layerObj = new GeoserverLayer(layer, new Legend(Drupal.settings.layers[layer].title), this.sessionLoadedLayers[layer].opacity);	
						layerObj.addSource();
					}
					else { //mapbox supercomposed layers
						layerObj = new MapboxLayer(layer, new Legend(Drupal.settings.layers[layer].title), this.sessionLoadedLayers[layer].opacity, this.mapboxLayers[layer]);
					}
					if (this.sessionLoadedLayers[layer].active) {
						this.addToMap(layerObj);
						this.activateLayer(layer);
						$("#" + layer + "-" + host).addClass("active"); 						
						$("#" + layer + "-" + host).parent().parent().next().toggleClass("hidden");			
					}
				}
			}
		}
	}

	class MapState {	
		constructor(loadedConfigId) {
			this.loadedConfig = loadedConfigId;
			this.includedTrees = []; 
			
			this.sessionLoaded = true;
			this.popupIdx = 0;
			this.selectedConfig = -1;

			//memoization of the publication and phenotype data requested from the server
			//helps to reduce the number of calls by storing previously requested results
			this.pubData = {};
			this.phenoData = {};
			this.trees = [];
			this.species = [];
		}

		loadActiveTrees(treesArray) {
			if (treesArray != undefined) {
				for (var i = 0; i < treesArray.length; i++) {
					activeTrees[treesArray[i]] = true;
				}
				this.includedTrees = treesArray;
			}
		}

		initMapProperties(zoom, pitch, bearing, center) {
			this.zoom = zoom;
			this.pitch = pitch;
			this.bearing = bearing;
			this.center = center;
		}		
	}

	loadSession();

	//load the layers and filters added in the previous session
	function loadSession(config = null, reload = false) {
		var session = Drupal.settings.session;
		if (config !== null) {
			session = config;
		}
		if (!reload) {
			mapState = new MapState(0);
			layersList = new LayersList(session.layers);
		}
	
		mapState.loadActiveTrees(session.included_trees);
		resetDatasets();

		mapState.initMapProperties(session.zoom, session.pitch, session.bearing, session.center);
			
		if (session.filters != null) {
			filterQuery = typeof(session.filters["query"]) == "object" ? session.filters["query"] : {};
			if (session.filters["active_sources"].length > 0) {
				for (var i = 0; i < session.filters["active_sources"].length; i++) {
					toggleDataset(parseInt(session.filters["active_sources"][i]), true);
				}	
			}
			else {
				toggleDataset(0, true);
				toggleDataset(1, true);
				toggleDataset(2, true);
			}
			if (Object.keys(filterQuery).length > 0) {
				$("#builder").queryBuilder("setRules", filterQuery);
			}
		}
		else {
			toggleDataset(0, true);
			toggleDataset(1, true);
			toggleDataset(2, true);
		}
		
		if (reload) {
			filterMap(true, false);
			positionMap();
		}
		/*
		if (reload) {
			layersList.initPreloaded();
			positionMap();
		}*/
	}
	
	/***************************************************************************************************

 	* Initialize the trees of the map and the methods associated with the trees on load of the webpage *

	***************************************************************************************************/
	//TODO: Split up to make maintaining and making changes easier
	window.onload = function () {
		map.on("load", function () {
			positionMap();
	
			//navigation controls
			map.addControl(new mapboxgl.NavigationControl());
			map.getCanvas().addEventListener("keydown", function(e) { 
				if (e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
					$("#arrow-controls").css("opacity", 1);
				}
				$("#arrow-controls").fadeTo("fast", 0.5);
            });

			//load the tree images
			loadImageWrapper(Drupal.settings.tree_img["exact"]["gymnosperm"], "gymnosperm_ex");
			loadImageWrapper(Drupal.settings.tree_img["exact"]["angiosperm"], "angiosperm_ex");
			loadImageWrapper(Drupal.settings.tree_img["approximate"]["gymnosperm"], "gymnosperm_app");
			loadImageWrapper(Drupal.settings.tree_img["approximate"]["angiosperm"], "angiosperm_app");
			loadImageWrapper(Drupal.settings.tree_img["treesnap"]["gymnosperm"], "treesnap_gymno");
			loadImageWrapper(Drupal.settings.tree_img["treesnap"]["angiosperm"], "treesnap_angio");	

			//intialize reusable mapbox popups
			var popupInfo = new mapboxgl.Popup();
			var popupHover = new mapboxgl.Popup({closeButton: false});

			//load all the tree datasets from treegenes, dryad, treesnap
			initMapTrees();

			// When a click event occurs on a feature in the trees layer, open a popup at the location of the feature
			// The popup will have a more detailed information about the trees
			map.on("click", function (e) {	
				//create a bounding box around the clicked point, to be used to query for features/trees around the bbox
				var treesBbox = [
					[e.point.x, e.point.y],
					[e.point.x, e.point.y]
				];

				//create a bounding box based on the lat/long coordinates of the clicked point, will be used to query for environmental data
				var bbox = (e.lngLat.lat - .1) + "%2C" + (e.lngLat.lng - .1) + "%2C" + (e.lngLat.lat + .1) + "%2C" + (e.lngLat.lng + .1);   

				if (isNeonSite(treesBbox)) {
					return;
				}

				//get all the trees around the bounding box if they exist        
				var treesInSameCoord = map.queryRenderedFeatures(treesBbox, {
					layers: [datasetLayerId]//treeDatasets.getActiveDatasets() 
				});

				var cluster = map.queryRenderedFeatures(treesBbox, {
					layers: [datasetClusterId]
				});
				
				//a tree wasn"t clicked, treat as random click event and display coords/env values only
				if (treesInSameCoord.length === 0 && cluster.length === 0) {		
					showEnvData(e.lngLat, bbox, treesBbox);
				}
				else if (treesInSameCoord.length > 0 && !treesInSameCoord[0].properties.cluster) {
					clickedTrees = [];
					for (var i = 0; i < treesInSameCoord.length; i++) {
						clickedTrees.push(treesInSameCoord[i].properties.id);
					}

					var coords = treesInSameCoord[0].geometry.coordinates;

					var el = document.createElement("div");
					el.className = "marker";
					el.style.backgroundImage = "url(" + Drupal.settings.tree_img["selected"] + ")";
					el.style.width = "24px";
					el.style.height = "24px"; 
					
					//  el.addEventListener("click", function() {
					//  window.alert(marker.properties.message);
					//  });
					//   
					
					if (currMarker != null) {
						currMarker.remove();
					}
					//add marker to map					
					currMarker = new mapboxgl.Marker(el)
						.setLngLat({"lng": coords[0], "lat": coords[1]})
						.addTo(map);

					//clickedTrees = treesInSameCoord;
					//var coordKey = coords[1] e.lngLat.lat + "_" + e.lngLat.lng;
					var coordKey = coords[1] + "_" + coords[0];
					showTreeDetails(coordKey);

					addEnvData("#tree-details-extra", bbox, map.queryRenderedFeatures(treesBbox));
				}
			});

			function renderNeonPopup(neonSite) {
				map.getCanvas().style.cursor = "pointer";
				var neonHTML = "<div class='text-center'><h3>Site Type: " + neonSite.properties.SiteType + "</h3><h5>";
				neonHTML += neonSite.properties.SiteHost + "<h5><h5>"; 
				neonHTML += neonSite.properties.DomainName + "</h5><div id='fieldsite-data'></div><p class='text-muted'>Lat:";
				neonHTML += neonSite.properties.Latitude + " | Long: " + neonSite.properties.Longitude + "</p>";
				
				return neonHTML;
			}

			// a special type of hover event for mapbox layers being tested
			// same ideas as before, but with different layer id and properties of the layer being shown
			map.on("mouseenter", layersList.neon[0], function (e) {
				var bbox = [
					[e.point.x, e.point.y],
					[e.point.x, e.point.y]
				];
				var neonSites = map.queryRenderedFeatures(bbox, {
					layers: [layersList.neon[0]]
				});

				console.log(neonSites);
				if (neonSites.length > 0) {
					popupHover.setLngLat(e.lngLat)
						.setHTML(renderNeonPopup(neonSites[0]))
						.addTo(map);
				}
				else {
					popupHover.remove();
				}
			});
			
			map.on("click", layersList.neon[0], function (e) {
				var bbox = [
					[e.point.x, e.point.y],
					[e.point.x, e.point.y]
				];
				var neonSites = map.queryRenderedFeatures(bbox, {
					layers: [layersList.neon[0]]
				});

				if (neonSites.length > 0) {
					map.getCanvas().style.cursor = "pointer";
					new mapboxgl.Popup().setLngLat(e.lngLat)
						.setHTML(renderNeonPopup(neonSites[0]))
						.addTo(map);

					//requesting additional data and the metadata for sites using this api url
					$.ajax({
						url: "https://phenocam.sr.unh.edu/api/cameras/?Sitename__contains=NEON." + neonSites[0].properties.PMC.substring(0,3) + "." + neonSites[0].properties.SiteID + "&format=json",
						dataType: "json",
						success: function (data, textStatus, jqXHR) {
							$("#fieldsite-data").html("");
							var fieldsiteHTML = "";
							var thisFieldsite = data.results[0];
							if (thisFieldsite.sitemetadata.flux_data) {
								fieldsiteHTML += "<h4>Flux Data: False</h4>";
							}
							else {
								fieldsiteHTML += "<h4>Flux Data: True</h4>";
								fieldsiteHTML += "<h5>Flux Networks: " + thisFieldsite.sitemetadata.flux_networks + "</h5>";
								fieldsiteHTML += "<h5>Flux Sitenames: " + thisFieldsite.sitemetadata.flux_sitenames + "</h5>";
							}						
							fieldsiteHTML += "<h4>Dominant Species: " + thisFieldsite.sitemetadata.dominant_species + "</h4>";
							fieldsiteHTML += "<h5>Primary Veg type: " + thisFieldsite.sitemetadata.primary_veg_type + " | Seconday Veg type: " + thisFieldsite.sitemetadata.secondary_veg_type + "</h5>";	
							fieldsiteHTML += "<h5>NA Ecoregion: " + thisFieldsite.sitemetadata.ecoregion + "</h5>";
							fieldsiteHTML += "<h5>WWF Biome: " + thisFieldsite.sitemetadata.koeppen_geiger + "</h5>";
							fieldsiteHTML += "<h5>Landcover igbp: " + thisFieldsite.sitemetadata.landcover_igbp + "</h5>";
							fieldsiteHTML += "<h5>Elevation: " + data.results[0].Elev + "</h5>";
							fieldsiteHTML += "<img style='width:100%' src='https://phenocam.sr.unh.edu/data/latest/" + thisFieldsite.Sitename + ".jpg'>";
							fieldsiteHTML += "<h5 class='text-muted'>Start: " + data.results[0].date_first + " | End: " + data.results[0].date_last + "</h5>";
							$("#fieldsite-data").append(fieldsiteHTML);
						},
						error: function (xhr, textStatus, errorThrown) {
							console.log({
								textStatus
							});
							console.log({
								errorThrown
							});
							console.log(xhr.responseText);
						}
					});
				}
			});

			map.on("mouseleave", layersList.neon[0], function () {
				map.getCanvas().style.cursor = "";
				popupHover.remove();
			});

			//hover event for neon plot sites
			map.on("mouseenter", layersList.neon[2], function (e) {
				var bbox = [
					[e.point.x, e.point.y],
					[e.point.x, e.point.y]
				];
				var neonPlots = map.queryRenderedFeatures(bbox, {
					layers: [layersList.neon[1], layersList.neon[2]]
				});
	
				console.log(neonPlots);

				if (neonPlots.length > 0) {
					var rawDate = neonPlots[0].properties.date.toString();
					console.log(rawDate);
					var date = rawDate.substring(0,4) + "-" + rawDate.substring(4,6) + "-" + rawDate.substring(6,8);
					map.getCanvas().style.cursor = "pointer";
					var neonHTML = "<div class='text-center'><h4>Plot Type: " + neonPlots[0].properties.plotTyp + "</h4><h5>";
					neonHTML += "National Land Cover DB classification: " + neonPlots[0].properties.nlcdCls + "<h5><h5>"; 
					neonHTML += "Subtype: " + neonPlots[0].properties.subtype + "</h5><h5 class='text-muted'>";
					neonHTML += "Date of collection: " + date + "</h5><p class='text-muted'>Lat:";
					neonHTML += neonPlots[0].properties.latitud + " | Long: " + neonPlots[0].properties.longitd + "</p>";
					//new mapboxgl.Popup().setLngLat(e.lngLat)
					popupHover.setLngLat(e.lngLat)
						.setHTML(neonHTML)
						.addTo(map);
				}
			});

			map.on("mouseleave", layersList.neon[2], function () {
				map.getCanvas().style.cursor = "";
				popupHover.remove();
			});
		
			layersList.initPreloaded();
		});
	};

	function loadImageWrapper(imagePath, imageName) {
		map.loadImage(imagePath, function (error, icon) {
			map.addImage(imageName, icon);
		});
	}

	function showEnvData(lngLat, bbox, treesBbox) {
		//create a dynamic html popup
		var popupHTML = "<div class='card' style='margin:0'><div class='card-header bg-secondary'>";
		popupHTML += "<h4 style='color: white'>Latitude: " + roundHundredths(lngLat.lat);
		popupHTML += " | " + "Longitude: " + roundHundredths(lngLat.lat) + "</h4></div>";
					
		//if there are any active layers on the map then add an environmental data section to the popup				
		if (layersList.getActiveLayers().length > 0) {
			popupHTML += "<h3 class='card-title'>Environmental Data:</h3><ul class='environmental-values list-group'></ul>";
			$(".environmental-values").html();
		}
		popupHTML += "</div>";

		//show the popup, then request the actual environmental data from geoserver
		new mapboxgl.Popup().setLngLat(lngLat).setHTML(popupHTML).addTo(map);
		addEnvData(".environmental-values", bbox, map.queryRenderedFeatures(treesBbox));
	}

	//tempt fix for showing the neon meta data
	function isNeonSite(treesBbox) {
		var neonSites = map.queryRenderedFeatures(treesBbox, {
			layers: [layersList.neon[0]]
		});

		return neonSites.length > 0;
	}

	function initMapTrees() {
		if (Object.keys(filterQuery).length == 0 && getActiveDatasets().length == 3) {
			getAllTrees(function(data) {
				addDatasetLayer(data);
				initMapSummary(data.length);
			});	
		}
		else {
			filterMap(false);
		}
	}

	/**
	 * Adds the layers for the tree dataset
	 * @param takes no arguments
	 * @return no return value
	*/
	function addDatasetLayer(trees) {
		map.addSource(datasetSourceId, {
			"type": "geojson",
			"data": {
				"type": "FeatureCollection",
				"features": trees
			},
			"cluster": true,
			"clusterMaxZoom": 9, // Max zoom to cluster points on
			"clusterRadius": 50 // Radius of each cluster when clustering points (defaults to 50)
		});

		map.addLayer({
			"id": datasetLayerId,
			"type": "symbol",
			"source": datasetSourceId,
			"filter": ["!", ["has", "point_count"]],
			"layout": {
				"icon-image": getTreeIcons(),
				//"icon-size": 0.04,
				"icon-allow-overlap": true,
			}, 
			}, "state-label", //set below text labels of states
		);

		//the cluster circles which will represent grouped up trees
		//color and size of the circle depends on cluster size, in arbitrary 0->250, 250->1000, 1000+ intervals
		map.addLayer({
			"id": datasetClusterId,
			"type": "circle",
			"source": datasetSourceId,
			"filter": ["has", "point_count"],
			"paint": {
				"circle-color": [
					"step",
					["get", "point_count"],
					clusterProps[0][1],
					clusterProps[1][0],	
					clusterProps[1][1],
					clusterProps[2][0],
					clusterProps[2][1],
				],
				"circle-radius": [
					"step",
					["get", "point_count"],
					25,
					clusterProps[1][0],
					35,
					clusterProps[2][0],
					45,
				],
			}
		});

		//the text displayed on top of the circles that give additional information like tree count in this cluster
		map.addLayer({
			"id": datasetClusterId + "-count",
			"type": "symbol",
			"source": datasetSourceId,
			"filter": ["has", "point_count"],
			"layout": {
				"text-field": "{point_count_abbreviated}" + " Trees",
				"text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
				"text-size": 12
			}
		});
		addDatasetClickEvents(datasetSourceId, datasetLayerId, datasetClusterId);
	}

	/**
	 * Makes an ajax call to the api to get all the tree ids associated with the dataset
	 * @param {function} handler - The function which the tree data will be passed to, to be processed
	*/
	function getAllTrees(handler) {
		$.ajax({
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/trees",
			dataType: "json",
			async: false,
			success: function (data) {
				handler(data);
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(xhr.responseText);
			}
		});
	}

	/**
	 * Retrieves the icons that is associated with each plant group and dataset
	 * @return an array representing the icons to be mapped
	 * format is [<icon type>, <icon val>, <icon_type>, <icon val>, ..., <default icon if tree matches with nothing>]
	*/
	function getTreeIcons() {		
		return [
			"match",
			["get", "icon_type"],
			0,
			"angiosperm_ex",
			1,
			"gymnosperm_ex",
			2,
			"angiosperm_app",
			3,
			"gymnosperm_app",
			4,
			"treesnap_gymno",
			5,
			"treesnap_gymno",
			"angiosperm_ex",
		];
	}

	/**
	 * Adds the click events of the dataset layers
	 * @param takes no arguments
	 * @return no return value
	*/
	function addDatasetClickEvents(sourceId, layerId, clusterId) {
		var treeHover = new mapboxgl.Popup({closeButton: false});
		map.on("mouseenter", layerId, function (e) {
			//create a bounding box around the clicked point
			var bbox = [
				[e.point.x, e.point.y],
				[e.point.x, e.point.y],
			];

			//get all the trees and their features that are within the bounding box 
			var treesInSameCoord = map.queryRenderedFeatures(bbox, {
				layers: [layerId],
			});
			
			var hoverText = treesInSameCoord.length == 1 ? treesInSameCoord.length + " Tree" : treesInSameCoord.length + " Trees";
			
			//if there are trees within the bounding box, then display the popup, else don"t display anything
			if (treesInSameCoord.length > 0) {
				map.getCanvas().style.cursor = "pointer";
				treeHover.setLngLat(e.lngLat)
					.setText(/*e.features[0].properties.species + " | " +*/ hoverText)
					.addTo(map);
			}
			else {
				treeHover.remove();
			}
		});

		// Change the cursor back to original state and remove any popup once it leaves the area of trees
		map.on("mouseleave", layerId, function () {
			map.getCanvas().style.cursor = "";
			treeHover.remove();
		});

		map.on("click", clusterId, function(e) {
			var bbox = [
				[e.point.x, e.point.y],
				[e.point.x, e.point.y],
			];

			var features = map.queryRenderedFeatures(bbox, {
				layers: [clusterId]
			});
			//var featureProps = ;
			console.log(features[0]);
			map.getSource(sourceId).getClusterExpansionZoom(features[0].properties.cluster_id, function(err, zoom) {
				if (err) {
					return;
				}
				map.easeTo({
					center: features[0].geometry.coordinates,
					zoom: zoom + 1,
				});
			});
		});	
	}

	function getActiveDatasets() {
		var activeList = [];
		for (var dataset in activeDatasets) {
			if (activeDatasets[dataset]) {
				activeList.push(dataset);
			}
		}
		return activeList;
	}	

	/**
	 * Updates the source of the dataset layers to the new dataset
	 * @param {array} newData - an array of tree features
	 * @return no return value
	*/
	function setData(newData) {
		map.getSource(datasetSourceId).setData({
  			"type": "FeatureCollection",
			"features": newData, 
		});
	}

	$(".carousel-control-prev").click(function() {
  		$("#tree-img-carousel").carousel("prev");
	});

	$(".carousel-control-next").click(function() {
		$("#tree-img-carousel").carousel("next");
	});

	function updateTreeImgs(imgs, species) {
		$("#tree-img-carousel .carousel-inner").html("");
		$("#tree-img-carousel .carousel-indicators").html("");
		//$("#tree-img-carousel").carousel("pause").removeData();
		$("#tree-imgs-container").html("");

		if (imgs == undefined || imgs.length == 0) {
			var speciesSplit = species.split(" ");
			var treeImg = speciesSplit[0].toLowerCase() + "_" + speciesSplit[1] + ".jpg";
			var imgFileName = Drupal.settings.basePath + "sites/default/files/treepictures/" + treeImg;
			$("#tree-img-carousel .carousel-indicators").eq(0).append("<li data-target='#tree-img-carousel' data-slide-to='0'></li>");
			$("#tree-img-carousel .carousel-inner").eq(0).append("<div class='carousel-item tree-img'><img class='d-block w-100' id='tree-view-img0' src='" + imgFileName + "' alt='First image'></div>");
			$("#tree-imgs-container").append("<div class='row'><div class='col img-full'><img src='" + imgFileName + "'/></div></div>");
		}	
		else {
			var row = "<div class='row'>";
			var numAddedElements = 0;
			for (var i = 0; i < imgs.length; i++) {	
				$("#tree-img-carousel .carousel-indicators").append("<li data-target='#tree-img-carousel' data-slide-to='" + i + "'></li>");
				$("#tree-img-carousel .carousel-inner").append("<div class='carousel-item tree-img'><img class='d-block w-100' id='tree-view-img" + i + "' src='" + imgs[i] + "' alt='tree img'></div>");	
				if (i % 2 == 0 && numAddedElements > 0) {
					$("#tree-imgs-container").append(row + "</div>");
					numAddedElements = 0;
					row = "<br /><div class='row'>";
				}
				row += "<div class='col img-full'><img src='" + imgs[i] + "'/></div>";
				numAddedElements++;
			}
			if (numAddedElements > 0) {
				$("#tree-imgs-container").append(row);
			}
			//$("#tree-imgs-container").append("</div>");
		}
		$(".carousel-item").first().addClass("active");
		$(".carousel-indicators > li").first().addClass("active");
	}

	function getTreegenesData(treeId) {
		$.ajax({
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/publications?api_key=" + Drupal.settings.ct_api + "&tree_acc=" + treeId.split("-")[0],
			dataType: "json",
			success: function (data) {
				if (data.length > 0) {
					$("#tree-pub-title").text(data[0].title);
					$("#tree-pub-author").text(data[0].author);
					$("#tree-pub-year").text(data[0].year);
					$("#tree-markers").text(data[0].markers);
					$("#tree-study-type").text(data[0].study_type);
					$("#tree-pub-link").text("View Additional Details");
					$("#tree-pub-link").attr("href", "https://treegenesdb.org/tpps_details/accession/" + data[0].accession);
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(xhr.responseText);
			}
		});	

		$.ajax({
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/phenotypes?api_key=" + Drupal.settings.ct_api + "&tree_id=" + treeId,
			dataType: "json",
			success: function (data) {
				console.log(data);
				var cvtermsSet = {};
				for (var i = 0; i < data.length; i++) {
					if (cvtermsSet[data[i].cvterm_name] == undefined) {
						$("#tree-phenotypes").append("    <span class='badge badge-success'>" + data[i].cvterm_name + "</span>");
						cvtermsSet[data[i].cvterm_name] = true;
					}
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(xhr.responseText);
			}
		});	

		/*
		$.ajax({
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/markertypes?api_key=" + Drupal.settings.ct_api + "&tree_id=" + treeId,
			dataType: "json",
			success: function (data) {
				console.log("MARKER TYPES");
				console.log(data);
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
					console.log(xhr.responseText);
			}
		});*/	
	}

	function resetTreeModalData() { 		
		$("#tree-pub-title").text(""); 		
		$("#tree-pub-author").text(""); 		
		$("#tree-pub-year").text(""); 		
		$("#tree-markers").text(""); 		
		$("#tree-pub-link").attr("href", "#"); 		
		$("#tree-pub-link").text("No Publication info found"); 		
		$("#tree-phenotypes").html(""); 	
	}


	function renderTreeDetails(data) {
		var sourceName = "TreeGenes";
		var treeId = data.uniquename;
			
		console.log(data);
		resetTreeModalData();

		if (data.source_id == 1) {
			sourceName = "TreeSnap";
			if (treeImgsStore[treeId] == undefined) {
				$.ajax({
					url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/tree/treesnap?api_key=" + Drupal.settings.ct_api + "&tree_id=" + treeId,
					dataType: "json",
					async: false,
					success: function (tsData) {
						$("#tree-submitter").text("Taken by: " + tsData.submitter);
						$("#tree-collection-date").text("Collection date: " + tsData.collection_date);
						updateTreeImgs(tsData.images.images, data.species);
						treeImgsStore[treeId] = {"submitter": tsData.submitter, "collection_data": tsData.collection_date, "images": tsData.images.images};
					},
					error: function (xhr, textStatus, errorThrown) {
						console.log({
							textStatus
						});
						console.log({
							errorThrown
						});
						console.log(xhr.responseText);
					}
				});	
			}
			else {	
				$("#tree-submitter").text("Taken by: " + treeImgsStore[treeId]["submitter"]);
				$("#treesnap-collection-date").text("Collection date: " + treeImgsStore[treeId]["collection_date"]);
				updateTreeImgs(treeImgsStore[treeId]["images"], data.species);
			}
			$("#all-imgs-title").text("Images contributed by TreeSnap user");
		}
		else {
			updateTreeImgs([], data.species);
			if(data.source_id == 2) {
				sourceName = "Data Dryad";
				//treeId = treeId.split("--")[1];
			}
			else {
				//get genotype, phenotype and publication data
				if (treeId.includes("TGDR")) {
					getTreegenesData(treeId);
				}
			}

			$("#all-imgs-title").text("TreeGenes Species Database Image");
			$("#tree-submitter").text("Species image from TreeGenesdb.org");
		}

		$("#tree-family").text(data.family == null ? "Unidentified" : data.family);
		$("#tree-plant-group").text(data.subkingdom == null ? "Unidentified" : data.subkingdom);
		$("#tree-species").text(data.species);
		$("#tree-source").text(sourceName);
		$("#tree-id").text(treeId);
		$("#tree-more-info-label").text(treeId);
		$("#tree-coord-type").text(data.coordinate_type == 0 ? "Exact" : "Approximate");
		$("#tree-coordinates").text(roundHundredths(data.latitude) + " Lat | " + roundHundredths(data.longitude) + " Long");
		
		$("#tpps-link").html("");
		if (sourceName == "TreeGenes" && treeId.includes("TGDR")) {
			$("#tpps-link").append("<a href='https://treegenesdb.org/tpps_details/accession/" + treeId.split("-")[0] + "' target='_blank'>Study TPPS Link</a>");
		}
	
		$("#tree-details").removeClass("hidden");
		$("#hide-tree-details").prop("disabled", false);
	}

	function getTreeData(treeId) {
		if (treeDataStore[treeId] == undefined) {	
			$.ajax({
				url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/tree?api_key=" + Drupal.settings.ct_api + "&tree_id=" + treeId,
				dataType: "json",
				async: false,
				success: function (data) {
					renderTreeDetails(data);
					treeDataStore[treeId] = data;
				},
				error: function (xhr, textStatus, errorThrown) {
					console.log({
						textStatus
					});
					console.log({
						errorThrown
					});
					console.log(xhr.responseText);
				}
			});		
		}
		else {
			renderTreeDetails(treeDataStore[treeId]);
		}
	}

	function showTreeDetails(coordKey) {
		getTreeData(clickedTrees[0]);
		$("#add-all-trees").removeClass("active");

		var activeTreesCount = 0;
		$("#tree-ids-list").html("");
		for (var i = 0; i < clickedTrees.length; i++) {
			var treeId = clickedTrees[i];
			var sourceSymbol = "TG";
			if (treeId.indexOf("treesnap") >= 0) {
				sourceSymbol = "TS";
			}
			else if (treeId.indexOf("dryad") >= 0) {
				sourceSymbol = "DD";
			}

			var activeClass = "";
			if (activeTrees[treeId] != undefined && activeTrees[treeId]) {
				activeClass = "active";
				activeTreesCount++;
			}
			
			$("#tree-ids-list").append("<div class='row justify-content-center'><button class='btn btn-primary tree-ids-select tree-selected' data-toggle='tooltip' data-placement='bottom' title='The ID of this tree.' id='" + clickedTrees[i] + "'>" + treeId + "</button><a class='btn btn-success add-tree " + activeClass + "' data-toggle='tooltip' data-placement='bottom' title='Add this tree for analysis.'> Add Tree</a>");/*<span class='badge badge-info' data-toggle='tooltip' data-placement='left' data-html='true' title='Tree data source. <br/> TG = TreeGenes <br/> TS = TreeSnap <br/> DD = DataDryad.'>" + sourceSymbol + "</span>");*/
		}

		if (activeTreesCount == clickedTrees.length) {
			$("#add-all-trees").addClass("active");
		}
		$("#tree-ids-list").first().children().children(":first").addClass("active");
	}

	function positionMap() {
		if (mapState.zoom != null) {
			map.setZoom(mapState.zoom);
			map.setPitch(mapState.pitch);
			map.setBearing(mapState.bearing);
			map.setCenter([mapState.center.lng, mapState.center.lat]);
		}
	}
	
	function resetFilters() {
		$("#builder").queryBuilder("reset");
		
		filterQuery = {};	
		getAllTrees(function(data) {
			setData(data);
			initMapSummary(data.length);
		});
		for (var k in datasetKey) {
			activeDatasets[datasetKey[k]] = true;
			$("#" + k +"-data").addClass("active");
		}	
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\

	function getClimUnits(valueName) { 			
		//a mapping of the name of the stored variables from the climate layers from geoserver to the appropriate units of measurement 			
		var climUnits = { 				
			"minimum_temperature": "°C", 				
			"maximum_temperature": "°C", 				
			"avg_temperature": "°C", 				
			"Precipitation": "mm", 				
			"Solar_radiation": "kJ m<sup>-2</sup> day<sup>-1</sup>", 				
			"wind_speed": "m s<sup>-1</sup>", 				
			"Water_vapor_pressure": "kPa", 			
		} 			
		return climUnits.hasOwnProperty(valueName) ? climUnits[valueName] : "";			
	}

	//requests the environmental data located at a particular point from geoserver
	function addEnvData(htmlEle, bbox, renderedFeatures) {
		$(htmlEle).html("");
		var baseUrl = "https://tgwebdev.cam.uchc.edu/geoserver/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetFeatureInfo&FORMAT=image%2Fpng&TRANSPARENT=true&QUERY_LAYERS=";
		// goes through each active layer, and adds the bounding box, and layer to be queried to the request url
		let activeLayers = layersList.getActiveLayers();
		for (var i = 0; i < activeLayers.length; i++) {
			var envLayer = Drupal.settings.layers[activeLayers[i]];
			console.log(envLayer);	
			if (envLayer.host === "0") {
				var getInfoUrl = baseUrl + envLayer.name + "&LAYERS=" + envLayer.name + "&INFO_FORMAT=application%2Fjson&I=128&J=128&WIDTH=256&HEIGHT=256&CRS=EPSG%3A4326&STYLES=&BBOX=" + bbox;
				$.ajax({
					url: getInfoUrl,
					dataType: "text",
					success: function(data) {
						var featureProps = JSON.parse(data).features[0].properties;
						//get the id of the layer requested from the url
						var thisLayer = Drupal.settings.fields[(this.url).split("QUERY_LAYERS=")[1].split("&LAYERS")[0]];

						//will check if the layer returned has a name already specified
						var layerName = thisLayer == undefined ? "" : thisLayer["Human-readable name for the layer"]

						//add the layer value and name to the detailed tree popup
						if (layerName !== "") {
							for (var k in featureProps) {
								var feature = featureProps[k];
								var units = getClimUnits(k);
								if (!isNaN(feature)) {
									feature = roundHundredths(feature);
								}
								var layerHTML = "<li class='list-group-item d-flex justify-content-between align-items-center '>" + layerName + "<span class='text-muted'>"; 
								layerHTML += feature + " " + units + "</span></li>";
								$(htmlEle).append(layerHTML);
							}	
						}
					},
					error: function (xhr, textStatus, errorThrown) {
						console.log({
							textStatus
						});
						console.log({
							errorThrown
						});
						console.log(xhr.responseText);
					}
				});
			}
			else if (envLayer.host === "1") {
				console.log(renderedFeatures);
				for (var l in renderedFeatures) {
					if (renderedFeatures.hasOwnProperty(l)) {
						for (var k in layersList.mapboxLayers) {
							for (var j = 0; j < layersList.mapboxLayers[k].length; j++) {
								if (renderedFeatures[l].layer.id == layersList.mapboxLayers[k][j]) {
									if (envLayer.title === "Major Soil Groups") {
										var soilHTML = "<li class='list-group-item d-flex justify-content-between align-items-center'>Soil Type <span class='text-muted'>";
										soilHTML += renderedFeatures[l].properties.value + "</span></li>";
										soilHTML += "<li class='list-group-item d-flex justify-content-between align-items-center'>Soil Descr <span class='text-muted' style='text-align:right'>";
										soilHTML += renderedFeatures[l].properties.grp_descr + "</span></li>";
										$(htmlEle).append(soilHTML);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	function updateMapSummary(numTrees, numSpecies, numPubs, numLayers) {
		console.log(numTrees + " " + numSpecies + " " + numPubs);
		if (numTrees != -1) {
			$("#num-trees").text(numTrees);
		}		
		if (numSpecies != -1) {
			$("#num-pubs").text(numSpecies);
		}		
		if (numPubs != -1) {
			$("#num-species").text(numPubs);
		}		
		if (numLayers != -1) {
			$("#num-layers").text(numLayers);;
		}		
	}

	function initMapSummary(numTrees) {
		$("#num-trees").text(numTrees);
		$.ajax({
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/stats/num/species",
			dataType: "json",
			success: function (data) {
				$("#num-species").text(data);
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(xhr.responseText);
			}
		});

		$.ajax({
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/stats/num/pub",
			dataType: "json",
			success: function (data) {
				$("#num-pubs").text(data);
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(xhr.responseText);
			}
		});
	}

	function filterNotification(result) {
		toastr.clear();
		if (result == 0) {
			toastr.error("Filter successful </br> No trees found that match those parameters.");
		}
		else{
			toastr.success("Filter successful </br> Result: " + result + " trees.");
		}
	}
	
	function filterMap(mapInit = true, move = true) {
		console.log("filtering");
		var active = getActiveDatasets();
		if (active.length == 0) {
			setData([]);
			toastr.success("No datasets active.");
		}
		else {
			if (Object.keys(filterQuery) == 0 && active.length == 3) {
				getAllTrees(function(data) {
					//addDatasetLayer(data);
					setData(data);
					var dataLength = data.length;
					initMapSummary(dataLength);
					filterNotification(dataLength);
				});	
			}
			else {
				var jsonData = {"query": filterQuery, "active_sources": active};
				//console.log(JSON.stringify(jsonData, null, 2));
				$.ajax({ 
					url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/trees/q?api_key=" + Drupal.settings.ct_api, 
					type: "POST",
					async: mapInit ? true : false,
					contentType: "application/json", 
					data: JSON.stringify(jsonData),
					success: function(res) {
						console.log(res);
						if (mapInit) {
							setData(res["features"]);
						}
						else {
							addDatasetLayer(res["features"]);
						}
						var lng = res["center"][0];
						var lat = res["center"][1]; 
						if (lng != null && lat != null) {
							console.log(res["center"]);
							//long must be between -180 to 180 and latitudes must be between -90 and 90
							if(lng < -180 || lng > 180 || lat > 90 || lat < -90) {
								toastr.error("Filtered trees don't have valid coordinates");
							}
							else {
								if (move) {	
									panToCenterTrees(res["center"]);
								}
								filterNotification(res["num_trees"]);
							}
						}		
						else {
							filterNotification(res["num_trees"]);
						}
						updateMapSummary(res["num_trees"], res["num_species"], res["num_pubs"]);
					},	
					error: function (xhr, textStatus, errorThrown) {
						console.log({
							textStatus
						});
						console.log({
							errorThrown
						});
						console.log(eval("(" + xhr.responseText + ")"));
						toastr.error("Filter failed");
					}
				});	
			}	
		}
	}

	function renderUserSessions(sessions) {
		for (var i = 0; i < sessions.length; i++) {
			//var dateParts = sessions[i]["created_at"].split("-");
			var jsDate = new Date(sessions[i]["updated_at"].replace(" ", "T")).toLocaleString();// new Date(dateParts[0], dateParts[1] - 1);
			var config = "<a href='#session-" + i + "' data-toggle='collapse' aria-expanded='false' class='list-group-item saved-session list-group-item-action flex-column align-items-start'>";
			config += "<div class='d-flex w-100 justify-content-between'><h4 class='mb-1 session-title'>" + sessions[i]["title"];
			config += "</h4><h5>" + jsDate + " <i class='fas fa-clock'></i></h5></div><div class='d-flex w-100 justify-content-between'><p class='mb-1'>";
			config += sessions[i]["comments"] + "</p></div></a><div class='row'><button type='button' class='btn btn-danger delete-saved-session' id='" + sessions[i]["session_id"] + "' >Delete</button><button class='btn btn-info share-saved-session'>Share</button></div></div></a>";
			//config += "<div id="" + configId + "" class="collapse bg-light" aria-expanded="false"><p><b>Species</b>: " + savedConfigs[i]["filters"]["organism"]["species"].toString() + "</p></div>";
			$("#saved-session-list").append(config);
		}
		if (sessions.length == 0) {
			$(".load-old-session").prop("disabled", true);
		}
		else {
			$(".load-old-session").prop("disabled", false);
		}
	};
	
	/**************************************************

	* ONCLICK EVENTS *

	**************************************************/
	$("#view-saved-session").on("click", function () {
		$("#saved-session-list").empty();
		$.ajax({
			method: "GET",
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/user/session/by-user/all?api_key=" + Drupal.settings.ct_api + "&user_id=" + Drupal.settings.user.user_id,
			dataType: "json",
			success: function (data) {
				Drupal.settings.user["sessions"] = data;
				renderUserSessions(data);
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(eval("(" + xhr.responseText + ")"));
			}
		});
		$("#saved-session").modal("toggle");
	});

	$(document).on("click", ".delete-saved-session", function () {
		var title = $(this).parent().prev().find("h4.session-title").text();
		console.log(title);
		console.log($(this)[0].id);
		if ($(this)[0].id == Drupal.settings.session.session_id) {
			alert("You can't delete the currently loaded session");
			return;
		}

		if (confirm("Are you sure you want to permanently remove '" + title + "'?")) {
			$(this).parent().prev().remove();
			$(this).next().remove();
			$(this).remove();
			$.ajax({
				method: "GET",
				url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/user/session/delete?api_key=" + Drupal.settings.ct_api + "&user_id=" + Drupal.settings.user.user_id + "&session_id=" + $(this)[0].id,
				success: function (data) {
					if ($("#saved-session-list").children().length == 0) {
						$("#load-old-session").prop("disabled", true);
					}
				},
				error: function (xhr, textStatus, errorThrown) {
					console.log({
						textStatus
					});
					console.log({
						errorThrown
					});
					console.log(eval("(" + xhr.responseText + ")"));
				}
			});
		}
	});

	$(document).on("click", ".share-saved-session", function () {
		var session_id = $(this).parent().children(":first")[0].id;
		alert("Link: " + getSessionUrl());
	});

	function notifyNumSelectedTrees() {
		var numSelectedTrees = mapState.includedTrees.length;
		var notificationStr = " trees currently selected.";
		if (numSelectedTrees == 1) {
			notificationStr = " tree currently selected.";
		}	
		toastr.info(numSelectedTrees + notificationStr);
	}

	$("#add-all-trees").on("click", function () {
		$(this).toggleClass("active");
		var isActive = $(this).hasClass("active");

		if (!isActive) {
			mapState.includedTrees = mapState.includedTrees.diff(clickedTrees);
			for (var i = 0; i < clickedTrees.length; i++) {
				activeTrees[clickedTrees[i]] = false;
			}
			$(".add-tree").removeClass("active");
		}
		else {
			mapState.includedTrees = unionArrays(mapState.includedTrees, clickedTrees);
			for (var i = 0; i < clickedTrees.length; i++) {
				activeTrees[clickedTrees[i]] = true;
			}
			$(".add-tree").addClass("active");
		}
		notifyNumSelectedTrees();
	});

	$(document).on("click", ".add-tree", function () {
		var treeId = $(this).parent().children(":first")[0].id;
	 	$(this).toggleClass("active");
	
		var isActive = $(this).hasClass("active");

		if (!isActive) {
			console.log("attempting to remove " + treeId);
			mapState.includedTrees.splice(mapState.includedTrees.indexOf(treeId), 1);
			activeTrees[treeId] = false;
			$("#add-all-trees").removeClass("active");
		}
		else {
			activeTrees[treeId] = true;
			mapState.includedTrees.push(treeId);
			var activeTreesCount = 0;
			var treesList = $("#tree-ids-list").children();
			for (var i = 0; i < treesList.length; i++) {
				if (treesList[i].children[1].className.includes("active")) {
					activeTreesCount++;
				}
			}
			if (activeTreesCount == clickedTrees.length) {
				$("#add-all-trees").addClass("active");
			}
		}
		notifyNumSelectedTrees();
	});

	$(document).on("click", ".tree-ids-select", function() {
		$(this).parent().parent().find("button.active").removeClass("active");
		$(this).addClass("active");
	
		console.log(this.id);	
		getTreeData(this.id);
	});

	$("#btn-get").on("click", function() {
		//toastr.clear();
		var result = $("#builder").queryBuilder("getRules");
		if (!$.isEmptyObject(result)) {
			toastr.info("Filtering...", "Applying Filter", {timeOut: 20000});
			filterQuery = result;
			filterMap();
		} else {
			alert("Can't apply empty filter arguments");
		}
		console.log(result);
	});

	// adding/removing a tree dataset and updating the current map state based on such an action
	$(".tree-dataset-btn").on("click", function () {
		//get the dataset id, and add/remove the dataset based on whether it"s currently active or not
		var datasetId = $(this)[0].id.split("-")[0];
		var active = $(this).hasClass("active");

		console.log(activeDatasets);

		if (!active) {
			activeDatasets[datasetKey[datasetId]] = false;
		}
		else {
			activeDatasets[datasetKey[datasetId]] = true;
		}
		toastr.clear();	
		toastr.info("Updating Datasets...", {timeOut: 10000});
		filterMap();
	});


	$("#btn-reset").on("click", function() {
		resetFilters();
	});

	$("#reset-map").on("click", function() {
		if (confirm("Are you sure you want to reset the map?")) {
			layersList.resetLayers();
			resetFilters();
			map.setZoom(4);
			map.flyTo({center: [-90, 40], bearing: 0, pitch: 0});
			$("#hide-tree-details").trigger("click", function(){});
		}
	});
	
	$("#hide-tree-details").on("click", function() {
		$("#tree-details").addClass("hidden");
		$(this).prop("disabled", true);
		if (currMarker != null) {
			currMarker.remove();
		}
	});

	//TODO: make this work better
	//updating the data values for the initial section of the analysis form when the analysis button is clicked
	$("#analysis-btn").on("click", function () {
		if ($(this).hasClass("disabled")) {
			alert("You must login to perform analysis");
			return;
		}
		else if (mapState.includedTrees.length <= 0) {
			alert("You have no trees selected for analysis");
			return;
		}
		
		$("#snp-chart").html("");

		// append the svg object to the body of the page
		$("#chart-loading").removeClass("hidden");	
		// get the data
		console.log(mapState.includedTrees);
		$.ajax({
			method: "POST",
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/trees/snp/missing",
			data: JSON.stringify({"trees": mapState.includedTrees}),
			contentType: "application/json", 
			success: function (data) {
				console.log(data);
				data = Object.values(data["snps_to_missing_freq"]/*data["tree_snp_missing"]*/);
				console.log(data);	
				renderChart(data, true);		
			},	
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(eval("(" + xhr.responseText + ")"));
			}
		});
	
		$("#analysis-form").modal();
		//createChart(700, 500, data);
	});
		
	function renderChart(data, newChart = false) {	
		// X axis: scale and draw:
		var height = chartProperties.chartHeight;
		var width = chartProperties.chartWidth;
		var margin = chartProperties.chartMargin;
		var svg = d3.select("#snp-chart")
		  .append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
		  .append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var x = d3.scaleLinear()
			.domain([0, 100])     // can use this instead of 1000 to have the max of data: d3.max(data, function(d) { return +d.price })
			.range([0, width]);
		
		svg.append("g")
			.attr("transform", "translate(0," + height + ")")
			.call(d3.axisBottom(x));

		// set the parameters for the histogram
		console.log(activeTrees);
		var histogram = d3.histogram()
			.domain(x.domain())  // then the domain of the graphic
			.thresholds(20); // then the numbers of bins

		// And apply this function to data to get the bins
		var bins = histogram(data);

		console.log(bins);
		// Y axis: scale and draw:
		var y = d3.scaleLinear()
			.range([height, 0]);
			y.domain([0, d3.max(bins, function(d) { return d.length; })]);   // d3.hist has to be called before the Y axis obviously
	  
		svg.append("g")
			.call(d3.axisLeft(y));

		svg.append("text")      // text label for the x axis
			.attr("x", width/2)
			.attr("y",  height + 35)
			.style("text-anchor", "middle")
			.text("% Missing");
		// append the bar rectangles to the svg element
		svg.selectAll("rect")
			.data(bins)
			.enter()
			.append("rect")
				.attr("x", 1)
				.attr("transform", function(d) { return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
				.attr("width", function(d) { return x(d.x1) - x(d.x0) -1 ; })
				.attr("height", function(d) { return height - y(d.length); })
				.style("fill", "#69b3a2")

		if (newChart) {				
			populateThresholdOpts(bins);
		}
		$("#chart-loading").addClass("hidden");
	}

	function populateThresholdOpts(bins) {
		$("#snp-threshold-missing").html("<option selected>% Missing</option>");
		for (var i = 0; i < Math.min(bins.length, 10); i++) {
			if (bins[i].length > 0) {
				var roundedVal = roundHundredths(bins[i][0]);
				$("#snp-threshold-missing").append("<option value='" + roundedVal + "'> < " + roundedVal + "%</option>");
			}
		}
	}

	$("#filter-chart").on("click", function() {
		var thresholdVal = $("#snp-threshold-missing").val();

		console.log(thresholdVal);

		$("#snp-chart").html("");	
		// get the data
		$("#chart-loading").removeClass("hidden");	
		$.ajax({
			method: "POST",
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/trees/snp/missing",
			data: JSON.stringify({"trees": mapState.includedTrees}),
			contentType: "application/json", 
			success: function (data) {
				console.log(data);
				data = Object.values(data["snps_to_missing_freq"]);
				var dataToKeep = [];
				for (var i = 0; i < data.length; i++) {
					if (data[i] <= thresholdVal) {
						dataToKeep.push(data[i]);
					}
				}
				console.log(dataToKeep);
				renderChart(dataToKeep);		
			},	
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(eval("(" + xhr.responseText + ")"));
			}
		});
	});

	function createChart(width, height, data) {
		$("svg").html("");	
		const svg = d3.select("svg");
		var margin = {
			top: 20,
			right: 0,
			left: 50,
			bottom: 30,
		};
		var x = d3.scaleBand()
		  .domain(data.map(d => d.name))
		  .range([margin.left, width - margin.right])
		  .padding(0.1)

		var y = d3.scaleLinear()
		  .domain([0, d3.max(data, d => d.value)]).nice()
		  .range([height - margin.bottom, margin.top]) 

		var xAxis = g => g
		  .attr("transform", `translate(0,${height - margin.bottom})`)
		  .call(d3.axisBottom(x).tickSizeOuter(0));

		var yAxis = g => g
		  .attr("transform", `translate(${margin.left},0)`)
		  .call(d3.axisLeft(y))
		  .call(g => g.select(".domain").remove());       

		svg.append("g")
		  .attr("fill", "steelblue")
		  .selectAll("rect")
		  .data(data)
		  .join("rect")
			.attr("x", d => x(d.name))
			.attr("y", d => y(d.value))
			.attr("height", d => y(0) - y(d.value))
			.attr("width", x.bandwidth())		  
			.on("mouseover", function() {
			  d3.select(this)
			  .attr("fill", "red");
			  })
			  /*
			.on("mouseout", function(d, i) {
				  d3.select(this).attr("fill", function() {
				  return "" + color(this.id) + "";
				  });
			 });*/

		svg.append("g")
			.call(xAxis)
			.append("text")             
			  .attr("transform",
				  "translate(" + (width/2) + " ," + 
								 (height + margin.top + 40) + ")")
			  .style("text-anchor", "middle")
			  .text("% missing");
		
		svg.append("g")
		  .call(yAxis)
		  .append("text")
			.attr("transform", "rotate(-90)")
			.attr("x",0 - (height / 2))
			.attr("dy", "1em")
			.style("text-anchor", "middle")
			.text("Frequency");

	}

	//Show the current attributes associated with each filter type for the map state selected by the user
	$(".analysis-options-section").on("click", function() {
		//if the user has previous saved config, then find the one that has been selected and is active
		if ($("#saved-config-searches").length) {
			var selectedConfig = $(".map-state-container").find("tr.active");
			if (mapState.selectedConfigId == selectedConfig.attr("name").split("-")[1]) {
				return;
			} 
			mapState.selectedConfigId = selectedConfig.attr("name").split("-")[1];
		}
		else {
			//selected config is the current one loaded	
			if (mapState.selectedConfigId == selectedConfig.attr("name").split("-")[1]) {
				return;
			}
			mapState.selectedConfigId = 0;
		}

		//initialize the properties of the selected config default to the current map state
		/*
		var configSelected = {
			"layers": layersList.getLayersToSave(),
			"filters": {
				"phenotype": filtersHandler.phenotypeSelected,
				"publications": filtersHandler.pubSelected,
				"organism": filtersHandler.organismSelected,
				"genotype": filtersHandler.genotypeSelected,
				"trees": {
					"num_trees": filtersHandler.treeFiltered.length,
					"trees_removed": mapState.excludedTrees,
					"publications": Object.keys(getPublications(filtersHandler.treeFiltered)),
				}
			},
		}*/

		//if the current map is not the selected one, then replace the configSelected value to the one selected
		if (mapState.selectedConfigId > 0) {
			configSelected = Drupal.settings.user_config_history[mapState.selectedConfigId-1];
		}

		//update the publication and layers section based on the selected config
		var pubContainerId = "map-publications-data";
		var layerContainerId = "map-environmental-data";
		
		//clear the sections
		$("#" + layerContainerId).html("");
		$("#" + pubContainerId).html("");

		//go through each publication and add the necessary information to the table
		for (var i = 0; i < configSelected.filters.trees.publications.length; i++) {
			$.ajax({
				method: "POST",
				url: Drupal.settings.basePath + "cartogratree/get/data/pub",
				dataType: "json",
				//async: false,
				data: {
					"data": JSON.stringify({"pub_acc": configSelected.filters.trees.publications[i]})
				},
				success: function (data) {
					console.log(data);
					var pubRow = "<tr id='" + data.id + "'>";
					pubRow += "<td>" + data.id + "</td>";
					pubRow += "<td>" + data.title + "</td>";
					pubRow += "<td>" + data.author + "</td>";
					pubRow += "<td>" + data.year + "</td>";
					//pubRow += "<td>" + data.species + "</td>";
					pubRow += "<td class='pub-num-trees'>" + data.num_trees + "</td>";
					pubRow += "<td>" + data.study_type + "</td>";
					pubRow += "<td><button type='button' data-toggle='button' class='btn btn-toggle pub-dataset-btn active' id='toggle-map-summary' aria-pressed='false' autocomplete='off'><div class='handle'></div></button>"
					pubRow += "</tr>";
					$("#" + pubContainerId).append(pubRow);
				},
				error: function (xhr, textStatus, errorThrown) {
					console.log({
						textStatus
					});
					console.log({
						errorThrown
					});
					console.log(xhr.responseText);
				}
			});
		}

		//go through each layer and add the necessary information for the active layers, show all the variables in the layer
		for (var layer in configSelected.layers) {
			console.log(layer);
			console.log(Drupal.settings.fields);
			if (Drupal.settings.layers.hasOwnProperty(layer) && layer in Drupal.settings.layers) {
				var layerName = Drupal.settings.layers[layer].name;
				if (layerName in Drupal.settings.fields) {
					var layerRow = "<tr id=analysis-" + layer + ">";
					layerRow += "<td>" + Drupal.settings.layers[layer].title + "</td>";
					layerRow += "<td>" + Drupal.settings.layers[layer].url + "</td>";
					for (var attribute in Drupal.settings.fields[layerName]) {
						console.log(attribute);
						if (attribute != "Layer ID" && attribute != "Human-readable name for the layer") {
							layerRow += "<td><input type='checkbox' name='layers-variables' value='" + attribute + "' checked>" + attribute + "</td>";
						}
					}
					layerRow += "</tr>";
					$("#" + layerContainerId).append(layerRow);
				}
			}
		}
	});

	//In the confirm section of the analysis form, show the data for the selected map state and the options the user has filtered out
	$(".analysis-confirm-section").on("click", function() {
		var configSelected = {
			"layers": layersList.getLayersToSave(),
			"filters": {
				/*
				"phenotype": filtersHandler.phenotypeSelected,
				"publications": filtersHandler.pubSelected,
				"organism": filtersHandler.organismSelected,
				"genotype": filtersHandler.genotypeSelected,
				"trees": {
					"num_trees": filtersHandler.treeFiltered.length,
					"trees_removed": mapState.excludedTrees, 
				}*/
			},
		}
		if (mapState.selectedConfigId > 0) {
			configSelected = Drupal.settings.user_config_history[mapState.selectedConfigId-1];
		}
				
		//var totalTrees = configSelected.filters.trees.num_trees;
		//if the user has deselected publications then update the total number of trees
		var numPublications = 0;
		$(".pub-dataset-btn").each(function() {
			if (!($(this).hasClass("active"))) {
				console.log($(this).parent().parent().children(":nth-child(5)").html());
				totalTrees -= $(this).parent().parent().children(":nth-child(5)").html();
			}
			else {
				numPublications++;
			}
		});
	
		$("#analysis-num-trees").text(totalTrees);
		$("#analysis-num-species").text(configSelected.filters.organism.species.length);
		$("#analysis-num-pub").text(numPublications);
	});

	//adding and removing layers from the map
	$(".layers-btn").on("click", function () {
		var layer = $(this)[0].id.split("-");
		var layerId = layer[0];
		var layerHost = layer[1];
		
		console.log(layerId);

		var layerNum = layerId.split("_")[2];

		//show opacity option and enable it for this layer
		$("#opacity-ctrl-" + layerNum).toggleClass("hidden");

		console.log($(this).hasClass("active"));
		
		console.log(layerId);
		//if layer has already been added and is active, then deactivate it
		if (!$(this).hasClass("active")) {
			layersList.deactivateLayer(layerId);
		}	
		else {
			var currentOpacity = parseInt($("#slider-" + layerNum + "-" + layerHost).val(), 10) / 100;
			if (layersList.getLayer(layerId) == null) {
				var layerObj;
				if (layerHost == 0) {
					layerObj = new GeoserverLayer(layerId, new Legend(Drupal.settings.layers[layerId].title), currentOpacity);
					layerObj.addSource();
				}
				else {
					layerObj = new MapboxLayer(layerId, new Legend(Drupal.settings.layers[layerId].title), currentOpacity, layersList.mapboxLayers[layerId]);
				}
				layersList.addToMap(layerObj);
			}
			layersList.activateLayer(layerId);
		}
	});

	// some stuff about the sidebar, resize the map whenever sidebar changes dimensions
	$(".show-layers-menu").on("click", function () {
		$("#layers-menu").toggleClass("hidden");
		$("#layers-menu-btn").toggleClass("active");
		map.resize();
	});

	$("[data-toggle=sidebar-collapse]").click(function () {
		sidebarCollapse();
	});

	function sidebarCollapse() {
		$(".menu-collapsed").toggleClass("d-none");
		$(".sidebar-submenu").toggleClass("d-none");
		$(".submenu-icon").toggleClass("d-none");
		$(".sidebar-container").toggleClass("sidebar-expanded sidebar-collapsed col-3 col-1");

		var separatorTitle = $(".sidebar-separator-title");
		if (separatorTitle.hasClass("d-flex")) {
			separatorTitle.removeClass("d-flex");
		}
		else {
			separatorTitle.addClass("d-flex");
		}

		$("#collapse-icon").toggleClass("fa-angle-double-left fa-angle-double-right");
		$("#layers-menu").addClass("hidden");
		$("#layers-menu-btn").removeClass("active");
		map.resize();
	}

	function saveSessionState() {
		var sessionState = {
			"session_id": Drupal.settings.session.session_id,
			"layers": layersList.getLayersToSave(),
			"filters": {"query": filterQuery, "active_sources": getActiveDatasets()},
			"user_data": {
				"center": map.getCenter(),
				"zoom": map.getZoom(),
				"pitch": map.getPitch(),
				"bearing": map.getBearing(),
				"included_trees": mapState.includedTrees,
			},
		};
		console.log({
			sessionState
		});
		console.log(JSON.stringify(sessionState));
		$.ajax({
			method: "POST",
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/user/session/save",
			contentType: "application/json", 
			data: JSON.stringify({"data": sessionState}),
			async: false,
			success: function (data) {
				console.log(data);
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log({
					textStatus
				});
				console.log({
					errorThrown
				});
				console.log(xhr.responseText);
			}
		});
	}

	function getCurrentUserSession() {
		$.ajax({
			method: "GET",
			async: false,
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/user/session/by-user?api_key=" + Drupal.settings.ct_api + "&user_id=" + Drupal.settings.user.user_id + "&session_id=" + Drupal.settings.session.session_id,
			success: function (data) {
				Drupal.settings.session.curr_sess_title = data.length > 0 ? data[0]["title"] : null;	
			}, 			
			error: function (xhr, textStatus, errorThrown) {
				console.log({textStatus});
				console.log({errorThrown});
				console.log(xhr.responseText);
			} 		
		});
		//return null;
	}

	function saveUserSession() {
		//get the title and comments entered by the user
		var title = $("#save-session-title").val();
		var comments = $("#save-session-comments").val();

		//title and login validation, only allow logged in users to save and the title field is required
		if (title.length <= 0) {
			alert("Title field must not be empty.");
			return;
		}
		
		var sessionData = {
			"title": title,
			"comments": comments,
			"user_id": Drupal.settings.user.user_id,
			"session_id": Drupal.settings.session.session_id,
		}

		getCurrentUserSession();
		console.log(Drupal.settings.session.curr_sess_title);
		if (Drupal.settings.session.curr_sess_title != null) {
			if(!confirm("Are you sure you want to overwrite the session titled '" + Drupal.settings.session.curr_sess_title + "'?")) {
				return;
			}
		}

		saveSessionState();

		$.ajax({
			method: "POST",
			url: "https://tgwebdev.cam.uchc.edu/cartogratree/api/v2/user/session/save/by-user",
			contentType: "application/json",
			data: JSON.stringify({"data": sessionData}),
			success: function (data) {
				var sessionLink = getSessionUrl();
				$(".session-url").text(sessionLink);
				$(".session-url").attr("href", sessionLink); 				
				$("#save-success-container").removeClass("hidden"); 
				$("#submit-user-session").addClass("hidden");
				$("#save-session-form").addClass("hidden");
			}, 			
			error: function (xhr, textStatus, errorThrown) {
				console.log({textStatus});
				console.log({errorThrown});
				console.log(xhr.responseText);
			} 		
		});
	}

	function getSessionUrl() {
		var sessionUrl = Drupal.settings.base_url + Drupal.settings.basePath +  "cartogratree";
		if (!sessionUrl.includes("session_id")) {
			sessionUrl += "?session_id=" + Drupal.settings.session.session_id;  
		}
		return sessionUrl;
	}

	function saveAnonSession() {
		var sessionUrl = getSessionUrl();
		$(".session-url").text(sessionUrl);
		$(".session-url").attr("href", sessionUrl);		
		$("#save-success").modal();
		
	}

	//Saving the current state of the map on click event
	$("#save-session").on("click", function () {
		if (Drupal.settings.user.logged_in) {
			//open save session popup dialog
			$("#save-success-container").addClass("hidden");
			$("#submit-user-session").removeClass("hidden");
			$("#save-session-form").removeClass("hidden");
			$("#save-user-session").modal();
		}	
		else {
			saveSessionState();
			saveAnonSession();
		}
	});
	
	$("#submit-user-session").on("click", function () {
		saveUserSession();
	});

	//loading previous saved configs by the user
	$(".load-old-session").on("click", function () {
		if ($("#saved-session-list").length) {
			var selectedConfig = $("#saved-session-list").find("a.active");
			console.log(selectedConfig);
			if (selectedConfig.length == 0) {
				alert("Please select a configuration");
			}
			else{
				var selectedSession = Drupal.settings.user["sessions"][selectedConfig.attr("href").split("-")[1]];
				loadSession(selectedSession, true);
				console.log("old sess: " + Drupal.settings.session.session_id);
				Drupal.settings.session.session_id = selectedSession["session_id"];
				console.log("new sess: " + Drupal.settings.session.session_id);
				document.getElementById("save-session-title").value = selectedSession["title"];
				document.getElementById("save-session-comments").value = selectedSession["comments"];
			}
		}
	});


	/**************************************************
	 
	* LEGENDS *
	 
	**************************************************/
	//Soil legend is created entirely with html, as opposed to requesting an image from geoserver
	function addSoilLegend(layerId, title) {
		var soilLayerId = "cartogratree_layer_4";
		var soilLayer = "ct:global_soils_merge_psql";
		$("#legend").removeClass("hidden").addClass("non-img-legend");
		var titleContainer = document.createElement("div");
		titleContainer.className = "block mb6";
		titleContainer.id = "legend-title-container";
		titleContainer.innerHTML = "<strong><a id='legend-title' href='#'>" + title + "</a></strong>";
		legend.appendChild(titleContainer);
		var contentContainer = document.createElement("div");
		contentContainer.id = "legend-content";
		var idx = 0;
		for (var section in Drupal.settings.soils) {
			//var section = sections[i];
			var color = Drupal.settings.soils[section][0]; //colors[i];
			var item = document.createElement("div");

			var key = document.createElement("span");
			key.className = "soil-legend-key";
			key.id = section + "-" + idx;
			key.style.backgroundColor = color;
			key.setAttribute("data-toggle", "tooltip");
			key.setAttribute("data-placement", "left");
			key.title = Drupal.settings.soils[section][1];
			var value = document.createElement("span");
			value.innerHTML = section;
			value.id = idx + "-soil-legend-label";
			value.className = "legend-label";

			item.appendChild(key);
			item.appendChild(value);
			contentContainer.appendChild(item);
			idx++;
		}
		legend.appendChild(contentContainer);

		$("#legend-title").on("click", function () {
			$("#legend-content").toggleClass("hidden");
		});

		//Whenever a soil type is clicked from the legend, we enable/disable it based on current active state
		$(".soil-legend-key").on("click", function () {
			var keyId = $(this)[0].id.split("-");
			var soilChoice = keyId[0];
			var sectionLayerId = soilLayerId + "_" + soilChoice;
			console.log(soilChoice);
			
			if (layersHandler.loadedSoil.indexOf(soilChoice) >= 0) {
				layersHandler.loadedSoil.splice(layersHandler.loadedSoil.indexOf(soilChoice), 1);
				$(this).removeClass("active");
				$("#" + keyId[1] + "-soil-legend-label").removeClass("active");
			}
			else{
				layersHandler.loadedSoil.push(soilChoice);
				$(this).addClass("active");
				$("#" + keyId[1] + "-soil-legend-label").addClass("active");
			}
			console.log(layersHandler.loadedSoil);
			var selectedSoils = layersHandler.loadedSoil.length == 0 ? null : ["match", ["get", "value"], layersHandler.loadedSoil, true, false]; 

			var mbLayers = layersList.mapboxLayers[layerId];
			for(var i = 0; i < mbLayers.length; i++){					
				map.setFilter(mbLayers[i], selectedSoils);
			}
		});
	}

	//custom legend for neon, which shows the four sites neon has tracked
	function addNeonLegend() {
		$("#legend").removeClass("hidden").addClass("non-img-legend");
		var titleContainer = document.createElement("div");
		titleContainer.className = "block mb6";
		titleContainer.id = "legend-title-container";
		titleContainer.innerHTML = "<strong><a id='legend-title' href='#'>Site Type</a></strong>";
		legend.appendChild(titleContainer);
		var contentContainer = document.createElement("div");
		contentContainer.id = "legend-content";

		var siteTypes = {"Core Aquatic":"#1f43c7", "Core Terrestrial":"#328128", "Relocatable Aquatic":"#8d9edd", "Relocatable Terrestrial":"#97bf92"};
		for (var site in siteTypes) {		
			if (siteTypes.hasOwnProperty(site)) {
				var item = document.createElement("div");

				var key = document.createElement("span");
				key.className = "neon-legend-key";
				key.style.backgroundColor = siteTypes[site];

				var value = document.createElement("span");
				value.innerHTML = site;
				value.className = "legend-label";

				item.appendChild(key);
				item.appendChild(value);
				contentContainer.appendChild(item);
			}
		}
		legend.appendChild(contentContainer);
    }

	function removeLegend() {
		$("#legend").addClass("hidden").removeClass("non-img-legend");
		while (legend.firstChild) {
			legend.removeChild(legend.firstChild);
		}
	}

	//opacity configuration
	$(".opacity").change(function (e) {
		console.log("updating opacity");
		var layerInfo = ($(this)[0].id).split("-");
		var layerId = "cartogratree_layer_" + layerInfo[1];
		if (layersList.isActive(layerId)) {
			var currVal = e.target.value;
			layersList.changeOpacity(layerId, parseInt(currVal, 10) / 100);
		}
	});

	// -- Loading spinner when requesting data using ajax, will stop spinning once all ajax calls are done
	$(document).ajaxStop(function() {
		$(".loading-spinner").addClass("hidden");
		$(".dynamic-data").removeClass("hidden");
	});

	function panToCenterTrees(target) {
		map.flyTo({
			center: target,
			bearing: 0,
			speed: .9,
			easing: function (t) {
				return t;
			}
		});
	}
	
	function isEmpty(obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				return false;
			}
		}
		return true;
	}	

	function toggleDataset(datasetId, state) {
		if (datasetId == 0) {
			activeDatasets[datasetId] = state;
			$("#treegenes-data").toggleClass("active");
		}
		else if (datasetId == 1) {
			activeDatasets[datasetId] = state;
			$("#treesnap-data").toggleClass("active");
		}
		else if (datasetId == 2) {
			activeDatasets[datasetId] = state;
			$("#datadryad-data").toggleClass("active");
		}
	}

	//update zoom value 
	map.on("zoom", () => {
	  $("#zoom-level").text(map.getZoom().toFixed(2));
	});

	Array.prototype.intersection = function (arr) {
		return this.filter(function (i) {
			return arr.indexOf(i) >= 0;
		});
	};	

	Array.prototype.diff = function (arr) {
		return this.filter(function (i) {
			return arr.indexOf(i) < 0;
		});
	};

	function unionArrays(x, y) {
		var hashmap = {};
		for (var i = 0; i < x.length; i++) {
     		hashmap[x[i]] = x[i];
		}
		for (var i = 0; i < y.length; i++) {
			hashmap[y[i]] = y[i];
		}

		var unionXY = [];
		for (var k in hashmap) {
			if (hashmap.hasOwnProperty(k)) {
      			unionXY.push(hashmap[k]);
			}
		}
		return unionXY;
	}

	function roundHundredths(doubleVal) {
		return Math.round(doubleVal * 1000) / 1000;
	}

	function resetDatasets() {
		$("#treegenes-data").removeClass("active");
		$("#treesnap-data").removeClass("active");
		$("#datadryad-data").removeClass("active");
	}
});
