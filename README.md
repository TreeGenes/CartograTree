[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3380534.svg)](https://doi.org/10.5281/zenodo.3380534)
# Brief description

CartograTree is a web-based application that allows researchers to identify, filter, compare, and visualize geo-referenced biotic and abiotic data. Its goal is to support numerous multi-disciplinary research endeavors including: phylogenetics, population structure, and association studies.

# Documentation
[CartograTree ReadTheDocs](https://cartogratree.readthedocs.io/en/latest/intro.html)

# Requirements
 * [GeoServer v2.11.2](http://docs.geoserver.org/2.11.2/user/installation/index.html#installation)
 * [Drupal 7](https://www.drupal.org/docs/7/install)
 * [Tripal v3](http://tripal.info/tutorials/v3.x/installation)
 * [Mapbox GL JS and access token](https://www.mapbox.com/mapbox-gl-js/api/)
